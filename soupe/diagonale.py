#!/usr/bin/env python3

H = 5
L = 4


def triangle(n):
    return (n * (n + 1)) // 2


def en_index(h, l):
    assert 0 <= h < H
    assert 0 <= l < L

    d = h + l
    retour = h + triangle(d)
    if d >= L:
        retour -= triangle(1 + (d - L))
    if d >= H:
        retour -= triangle(d - H)
    return retour


def en_position(i):
    assert 0 <= i < H * L

    mins = list()
    hs = list()
    h = 0
    for l in range(L):
        mins.append(en_index(h, l))
        hs.append(h)
    l = L - 1
    for h in range(1, H):
        mins.append(en_index(h, l))
        hs.append(h)

    # On cherche l'indice maximal tel que mins[d] <= i
    d_min = 0
    d_max = len(mins)
    while d_max - d_min > 1:
        d = (d_min + d_max) // 2
        if mins[d] <= i:
            d_min = d
        else:
            d_max = d

    h = i - mins[d_min] + hs[d_min]
    l = d_min - h

    return h, l


def balayer():
    k = 0
    for d in range(H + L):
        for h in range(d + 1):
            l = d - h
            if 0 <= h < H and 0 <= l < L:
                i = en_index(h, l)
                if i != k:
                    print(f"({l}, {h}) → {k} | en_index() = {i}")
                h2, l2 = en_position(k)
                if not (h2 == h and l2 == l):
                    print(f"{k} → ({l}, {h}) | en_position() = ({l2}, {h2})")

                k += 1


if __name__ == "__main__":
    balayer()
