use std::fs::File;
use std::io::Read;
use std::io::BufReader;

fn v1() {
    let mut fichier = File::open("fichier.rs").unwrap();
    let mut tampon = [0; 8];
    let mut q = 0;
    let mut r = 0;

    loop {
        let taille = fichier.read(&mut tampon);
        match taille {
            Err(e) => {
                println!("{:?}", e);
                break;
            }
            Ok(t) => {
                if t == 0 {
                    println!("Cette fois, c'est fini");
                    break;
                } else if t != tampon.len() {
                    println!("Ça sent la fin ! : {} octet(s) restant(s)", t);
                    r = t;
                } else {
                    q += 1;
                }
            }
        }
    }

    println!(
        "Il a fallu {} blocs de {} octets et un reliquat de {} octets",
        q,
        tampon.len(),
        r
    );
}

fn v2(capacité: usize) {
    let mut fichier = File::open("fichier.rs").unwrap();
    let mut tampon = vec![0; capacité];
    let mut q = 0;
    let mut r = 0;

    loop {
        let taille = fichier.read(tampon.as_mut_slice());
        match taille {
            Err(e) => {
                println!("{:?}", e);
                break;
            }
            Ok(t) => {
                if t == 0 {
                    println!("Cette fois, c'est fini");
                    break;
                } else if t != capacité {
                    println!("Ça sent la fin ! : {} octet(s) restant(s)", t);
                    r = t;
                    println!("Mais le tampon fait toujours {} octet(s)", tampon.len());
                } else {
                    q += 1;
                }
            }
        }
    }

    println!(
        "Il a fallu {} blocs de {} octets et un reliquat de {} octets",
        q,
        capacité,
        r
    );
}

fn v3(capacité: usize) {
    let fichier = File::open("fichier.rs").unwrap();
    let mut lecteur = BufReader::with_capacity(4 * capacité, fichier);
    let mut tampon = vec![0; capacité];
    let mut q = 0;
    let mut r = 0;

    loop {
        let taille = lecteur.read(tampon.as_mut_slice());
        match taille {
            Err(e) => {
                println!("{:?}", e);
                break;
            }
            Ok(t) => {
                if t == 0 {
                    println!("Cette fois, c'est fini");
                    break;
                } else if t != capacité {
                    println!("Ça sent la fin ! : {} octet(s) restant(s)", t);
                    r = t;
                    println!("Mais le tampon fait toujours {} octet(s)", tampon.len());
                } else {
                    q += 1;
                }
            }
        }
    }

    println!(
        "Il a fallu {} blocs de {} octets et un reliquat de {} octets",
        q,
        capacité,
        r
    );
}



fn main() {
    v1();
    v2(7);
    v3(9);
}
