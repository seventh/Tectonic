use std::env;

pub mod tectonic;

use tectonic::fichier_001::Lecteur;


fn main() {
    // Lecture code par code
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Usage : lecteur <fichier.bin>");
    }
    else {
        let lecteur = Lecteur::new(&args[1]);
        match lecteur {
            Err(e) => {
                println!("Le fichier {:?} n'est pas reconnu : {:?}", args[1], e);
            }
            Ok (lecteur) => {
                for code in lecteur {
                    println!("{:?}", code);
                }
            }
        }
    }
}
