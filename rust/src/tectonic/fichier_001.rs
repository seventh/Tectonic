use rug::Integer;
use rug::integer::Order;
use std::fs::File;
use std::io;
use std::io::BufReader;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Read;
use std::iter::Iterator;

use crate::tectonic::BaseLinéaire;

pub struct Segment {
    /// Nombre d'octets occupés par chaque entrée
    pub taille: usize,
    /// Nombre d'entrées
    pub nombre: usize,
}


// Pour pouvoir implémenter le trait 'Iterator', il faut rendre silencieuses
// les erreurs de format lors des itérations. Alors, quitte à opérer ainsi, on
// se dit «autant être homogène». Cependant, pour que la base soit toujours
// disponible, le constructeur doit pouvoir remonter une erreur.

pub struct Lecteur {
    pub base: BaseLinéaire,
    pub erreur: Option<Error>,
    /// Lecteur de fichier avec tampon intermédiaire pour limiter les appels
    /// systèmes
    flux: BufReader<File>,
    fin_rencontrée: bool,
    blocs: Vec<Segment>,
}

impl Lecteur {
    pub fn new(chemin: &str) -> io::Result<Lecteur> {
        let marque: &[u8] = b"TECTONIC\x01";
        let flux = File::open(chemin)?;
        let mut lecteur = BufReader::new(flux);
        let mut entete: [u8; 20] = [0; 20];

        lecteur.read(&mut entete)?;

        if &entete[0..marque.len()] != marque {
            return Err(Error::new(ErrorKind::InvalidData, "Format non supporté"));
        }

        let base = BaseLinéaire::new(entete[9].into(), entete[10].into(), entete[11].into());

        Ok(Lecteur {
            base: base,
            erreur: None,
            flux: lecteur,
            fin_rencontrée: false,
            blocs: Vec::new(),
        })
    }

    fn _charger_blocs(&mut self) {
        let nb_codes: usize = {
            let mut tampon: [u8; 1] = [0; 1];
            let statut = self.flux.read_exact(&mut tampon);
            if statut.is_err() {
                self.erreur = Some(statut.unwrap_err());
                return;
            }
            if tampon[0] == 255 {
                self.fin_rencontrée = true;
                return;
            }
            tampon[0].into()
        };

        let mut tampon: [u8; 5] = [0; 5];
        for _ in 0..nb_codes {
            let statut = self.flux.read_exact(&mut tampon);
            if statut.is_err() {
                self.erreur = Some(statut.unwrap_err());
                return;
            }
            let nb_octets: usize = tampon[0].into();
            let quatre_octets: &[u8; 4] = tampon[1..5].try_into().unwrap();
            // Certains microcontrôleurs pourraient avoir usize = u2…
            let nb_codes: usize = u32::from_be_bytes(*quatre_octets).try_into().unwrap();
            self.blocs.push(Segment {
                nombre: nb_codes,
                taille: nb_octets,
            });
        }
    }
}

impl Iterator for Lecteur {
    type Item = Integer;

    fn next(&mut self) -> Option<Integer> {
        if self.erreur.is_some() || self.fin_rencontrée {
            return None;
        }
        while self.blocs.is_empty() && self.erreur.is_none() && !self.fin_rencontrée {
            self._charger_blocs();
        }
        if self.blocs.is_empty() {
            return None;
        }

        let segment = &mut self.blocs[0];
        let mut tampon: Vec<u8> = vec![0; segment.taille];
        let statut = self.flux.read_exact(&mut tampon);
        if statut.is_err() {
            self.erreur = Some(statut.unwrap_err());
            return None;
        }
        segment.nombre -= 1;
        if segment.nombre == 0 {
            self.blocs.remove(0);
        }
        Some(Integer::from_digits(&tampon[..], Order::Msf))
    }
}


pub struct Écrivain {
}

impl Écrivain {
    pub fn new(chemin: &str) -> Self {
        Self {}
    }
}
