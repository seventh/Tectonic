pub mod fichier_001;

/// Caractéristiques d'une Grille classique de Tectonic

#[derive(Debug)]
pub struct Dimensions {
    /// Nombre de cases en hauteur
    pub hauteur: usize,
    /// Nombre de cases en largeur
    pub largeur: usize,
    /// Valeur maximale possible pour une case
    pub maximum: usize,
}

impl Dimensions {
    pub fn new(
        hauteur: usize,
        largeur: usize,
        maximum: usize,
    ) -> Self {
        assert!(hauteur > 0);
        assert!(largeur > 0);
        assert!(maximum > 0);

        Self {
            hauteur: hauteur,
            largeur: largeur,
            maximum: maximum,
        }
    }

    /// Les coordonnées désignent-elles un point de la Grille ?
    pub fn est_valide(&self, hauteur: usize, largeur:usize) -> bool {
        hauteur < self.hauteur && largeur < self.largeur
    }

    /// Nombre de cases de la Grille
    pub fn nb_cases(&self) -> usize {
        self.hauteur * self.largeur
    }

    pub fn transposer(&mut self) {
        (self.hauteur, self.largeur) = (self.largeur, self.hauteur);
    }
}

/// Conversion entre systèmes de coordonnées 2D ←→ 1D

pub trait Base {

    /// Conversion de 2D vers 1D
    fn image(&self, hauteur: usize, largeur:usize) -> usize;

    /// Conversion de 1D vers 2D
    fn antécédent(&self, index: usize) -> (usize, usize);
}

/// Conversion ligne à ligne
pub struct BaseLinéaire {
    /// Système de référence
    pub base: Dimensions,
}

impl BaseLinéaire {
    pub fn new(hauteur: usize, largeur:usize, maximum:usize) -> Self {
        Self {
            base: Dimensions::new(hauteur, largeur, maximum),
        }
    }
}

impl Base for BaseLinéaire {
    fn image(&self, hauteur: usize, largeur: usize) -> usize {
        assert!(self.base.est_valide(hauteur, largeur));

        self.base.largeur * hauteur + largeur
    }

    fn antécédent (&self, index: usize) -> (usize, usize) {
        assert!(index < self.base.nb_cases());

        let largeur = index % self.base.largeur;
        let hauteur = index / self.base.largeur;

        (hauteur, largeur)
    }
}

/// Une case est modélisée comme l'association de deux paramètres indépendants.
/// En effet, dans un puzzle, toutes les valeurs ne sont pas connues. Ou encore,
/// la structure de la grille est à découvrir dans Tiwanaku, avant toute chose.
#[derive(Clone, Copy, Default)]
pub struct Case {
    /// Les valeurs vont de 1 à base.maximum inclus
    valeur : Option<usize>,
    /// Identifiant de Bloc
    bloc: Option<usize>,
}


pub struct Bloc {
    pub grille: Grille,
    pub id: usize,
}


pub struct Grille {
    pub base: BaseLinéaire,
    cases : Vec<Case>,
}

impl Grille {

    pub fn new(hauteur: usize, largeur: usize, maximum: usize) -> Grille {
        let base = BaseLinéaire { base : Dimensions::new(hauteur, largeur, maximum), };
        let nb_cases = base.base.nb_cases();
        let cases = vec![Case::default(); nb_cases];

        Grille { base : base,
                 cases : cases,
        }
    }

    pub fn case(&self, hauteur : usize, largeur:usize) -> &Case {
        let index = self.base.image(hauteur, largeur);
        &self.cases[index]
    }

    pub fn case_mut (&mut self, hauteur:usize, largeur:usize) -> &mut Case {
        let index = self.base.image(hauteur, largeur);
        &mut self.cases[index]
    }
}
