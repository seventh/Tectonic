#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Charge et résout une grille de Tectonic/Kemaru/Sùguru

Le but n'est pas de faire une recherche brute mais d'utiliser des
techniques habituelles afin d'évaluer la difficulté

Attention : le Kemaru ferait également usage d'opérateurs mathématiques.
Dans ce cas, ce serait un tout autre jeu.

https://sudoku.megastar.fr/accueil/techniques-de-tectonic/
https://hodoku.sourceforge.net/en/techniques.php
https://sudopedia.org/wiki/Main_Page
"""

import collections
import itertools
import logging

from commun import Configuration
from tectonic.serial import Codec


class CaseNavigable:
    """La valeur d'une CaseNavigable évolue dans le temps

    On passe d'un ensemble de valeurs possibles à une valeur définitive
    """

    def __init__(self, position, valeur, région):
        self.position = position
        self.région = région
        if valeur > 0:
            self.libres = None
            self.valeur = valeur
        else:
            self.libres = set()
            self.valeur = None

        # Dans les huit directions
        self.voisins = list()

    def __repr__(self):
        return (f"{self.__class__.__name__}[0x{id(self):x}]"
                f"(position={self.position})")

    def __str__(self):
        if self.valeur is not None:
            return str(self.valeur)
        else:
            return str(self.libres)

    def fixer(self, valeur=None):
        if valeur is None:
            assert len(self.libres) == 1
            self.valeur = self.libres.pop()
        else:
            assert valeur in self.libres
            self.valeur = valeur

        for autre in self.région.cases:
            if autre is not self and autre.libres is not None:
                autre.libres.discard(self.valeur)
        for autre in self.voisins:
            if autre.libres is not None:
                autre.libres.discard(self.valeur)

        self.libres = None

    def est_fixée(self):
        return self.valeur is not None

    def rejeter(self, valeur):
        # 'remove' plutôt que 'discard' pour vérifier l'appartenance
        self.libres.remove(valeur)
        if len(self.libres) == 1:
            self.fixer()


class RégionNavigable:
    """Ensemble des cases d'un bloc
    """

    def __init__(self, id):
        self.id = id
        self.cases = list()

    def __len__(self):
        return len(self.cases)

    def forcés(self):
        """Valeurs fixées
        """
        return set([c.valeur for c in self.cases if c.valeur is not None])

    def libres(self):
        """Valeurs non encore fixées
        """
        return set(range(1, len(self) + 1)).difference(self.forcés())


class GrilleNavigable:

    def __init__(self, grille):
        self.base = grille.base
        self.régions = dict()
        self.cases = [None] * len(grille.cases)
        for i, case in enumerate(grille.cases):
            région = self.régions.setdefault(case.région,
                                             RégionNavigable(case.région))
            position = grille.base.en_position(i)
            self.cases[i] = CaseNavigable(position, case.valeur, région)
            région.cases.append(self.cases[i])

        # Voisinages
        for i, case in enumerate(self.cases):
            h, l = self.base.en_position(i)
            self._ajouter_voisin(case, h + 1, l)
            self._ajouter_voisin(case, h + 1, l + 1)
            self._ajouter_voisin(case, h + 1, l - 1)
            self._ajouter_voisin(case, h, l + 1)
            self._ajouter_voisin(case, h, l - 1)
            self._ajouter_voisin(case, h - 1, l)
            self._ajouter_voisin(case, h - 1, l + 1)
            self._ajouter_voisin(case, h - 1, l - 1)

        # Initialisation des possibles
        for région in self.régions.values():
            for case in région.cases:
                if case.valeur is None:
                    case.libres = région.libres()
                    for v in case.voisins:
                        case.libres.discard(v.valeur)

    def __str__(self):
        # On détermine la longueur des champs à afficher
        lgv = max([len(str(c)) for c in self.cases], default=1)
        lgc = lgv

        # Constitution de la grille, ligne à ligne
        lignes = list()
        for h in range(self.base.hauteur):
            # Séparateur horizontal
            ligne = "+"
            for l in range(self.base.largeur):
                séparateur = " "
                if h == 0:
                    séparateur = "-"
                else:
                    r = self[(h, l)].région
                    if r.id >= 0 and self[(h - 1, l)].région.id != r.id:
                        séparateur = "-"
                ligne += séparateur * lgc + "+"
            lignes.append(ligne)

            # Cases
            ligne = str()
            for l in range(self.base.largeur):
                case = self[(h, l)]
                séparateur = " "
                if l == 0:
                    séparateur = "|"
                else:
                    r = case.région
                    if r.id >= 0 and self[(h, l - 1)].région.id != r.id:
                        séparateur = "|"
                ligne += séparateur
                v = f"{str(case):^{lgv}}"
                ligne += v
            ligne += "|"
            lignes.append(ligne)

        # Dernier séparateur horizontal
        ligne = "+" + ("-" * lgc + "+") * self.base.largeur
        lignes.append(ligne)

        retour = "\n".join(lignes)
        return retour

    def __getitem__(self, position):
        h, l = position
        index = self.base.en_index(hauteur=h, largeur=l)
        return self.cases[index]

    def __setitem__(self, position, valeur):
        h, l = position
        index = self.base.en_index(hauteur=h, largeur=l)
        self.cases[index] = valeur

    def _ajouter_voisin(self, case, h, l):
        """Ajoute la case de coordonnées (h, l) aux voisins de la case
        si ces coordonnées sont valides
        """
        if self.base.est_valide(hauteur=h, largeur=l):
            case.voisins.append(self[(h, l)])

    def est_complète(self):
        for case in self.cases:
            if not case.est_fixée():
                return False
        return True


# Association d'une valeur et d'une case (position)
# La Proposition(c,v) est la modélisation de l'affirmation
# «Il y a un 'v' dans la case 'c'.»

Proposition = collections.namedtuple("Proposition", "case valeur")

# De nombreuses techniques de résolution humaines s'appuient sur ce modèle,
# en mettant en place des liens entre deux propositions A et B:
#
# - lien faible : A et B ne peuvent pas être vraies simultanément (mais toutes
# deux peuvent être fausses)
# - lien fort : seule A ou B est vraie
#
# À noter qu'un lien fort est également faible.
#
# Les techniques de résolution à base de Proposition cherchent à enchaîner les
# liens pour déduire une information.
#
# Un enchaînement entre deux liens forts F(A,B) et F(B, C) peut être exploité
# de différentes manières :
#
# - Si A, alors !B, alors C
# - Si !A, alors B, alors !C
#
# Un enchaînement entre un lien fort F(A,B) et un lien faible f(B, C) peut
# également être exploité de deux manières, mais dans cet ordre (d'abord le
# lien fort, puis le lien faible) :
#
# - Si !A, alors B, alors !C
# - Si C, alors !B, alors A
#
# Par contre, il n'y a pas d'exploitation possible entre deux liens faibles
# f(A,B) et f(B,C) :
#
# - Si A, alors !B. Mais C peut encore être fausse.
# - Si C, alors !B. Mais A peut encore être fausse.
#
# Quelque soit la nature des Propositions exploitées, on cherche
# systématiquement à atteindre une contradiction :
#
#   f(A,B) F(B,C) f(C,A)
# Si A, alors !B, alors C, alors !A. En résumé, si A est vraie, alors A est
# fausse. La conclusion logique est que A ne peut pas être vraie. A est donc
# fausse.
#
# En généralisant, on obtient une contradiction à chaque fois qu'il existe une
# boucle de longueur impaire sur une Proposition. Les différentes techniques de
# résolution différent de par la nature des Propositions qu'elles utilisent :
#
# X-chaîne : Toutes les Propositions concernent la même valeur tout le long de
# la chaîne
# XY-chaîne : Toutes les valeurs sont autorisées


class Nœud:

    def __init__(self, proposition):
        # Association Nœud → Proposition
        self.proposition = proposition

        # Liens - listes de Nœuds
        self.durs = list()
        self.mous = list()

    @property
    def liens(self):
        return self.durs + self.mous


class Raisonnement(list):

    def __str__(self):
        lignes = list()
        for i, p in enumerate(self):
            if i == 0:
                lignes.append(f"Si «{p.proposition}» est vraie")
            elif i % 2 == 0:
                lignes.append(f"Alors {p.proposition} est vraie")
            elif i == len(self) - 1:
                lignes.append(f"Alors {p.proposition} est fausse")
        return "\n".join(lignes)

    def est_bouclé(self):
        """Un Raisonnement est circulaire si les première et dernière
        Propositions sont identiques
        """
        return (len(self) >= 2 and self[0] is self[-1])

    def est_contradictoire(self):
        """Un Raisonnement est une contradiction quand :
        - il est de longueur paire
        - les première et dernière Propositions sont identiques
        """
        lg = len(self)
        return (lg >= 2 and lg % 2 == 0 and self[0] is self[-1])

    def développer(self, proposition):
        """Étend le raisonnement avec une nouvelle proposition si possible
        """
        retour = None
        if proposition not in self[1:]:
            retour = Raisonnement(Raisonnement(self) + [proposition])
        return retour


class Logigraphe:
    """Un Logigraphe modélise l'approche humaine des questions logiques.

    Pour cela, il établit des liens entre propositions, et cherche des
    contradictions afin d'invalider certaines d'entre elles.
    """

    def __init__(self):
        # Association Proposition → Nœud
        self.nœuds = dict()

    def créer_lien_dur(self, pa, pb):
        """Un lien logique dur («strong link») entre propositions peut être
        établi si seule l'une des deux propositions est vraie.

        Un lien dur entre propositions A et B peut donc être exploité de deux
        façons :

        - Si A, alors ̶B
        - Si —B, alors A

        de façon symétrique entre A et B.

        À noter qu'un lien dur est également mou.
        """
        na = self.nœuds.setdefault(pa, Nœud(pa))
        nb = self.nœuds.setdefault(pb, Nœud(pb))
        na.durs.append(nb)
        nb.durs.append(na)

    def créer_lien_mou(self, pa, pb):
        """Un lien logique mou («weak link») entre propositions peut être
        établi si le fait qu'une proposition soit vraie invalide l'autre. Il se
        peut très bien qu'aucune des deux propositions ne soit vraie.

        Un lien mou entre propositions A et B est exploité de deux façons :

        - Si A, alors ̶B
        - Si B, alors —A
        """
        na = self.nœuds.setdefault(pa, Nœud(pa))
        nb = self.nœuds.setdefault(pb, Nœud(pb))
        na.mous.append(nb)
        nb.mous.append(na)

    def identifier_contradictions(self):
        """Itérateur de Raisonnements qui, tous, révèlent une contradiction.

        Une contradiction est un Raisonnement :
        - de longueur paire ;
        - dont l'hypothèse et la conclusion portent sur la même proposition.

        Ainsi, une contradiction donne : Si A est vraie, …, alors A est fausse.
        Donc A ne peut pas être vraie. Elle est donc fausse.
        """
        # Afin de favoriser les Raisonnements les plus concis :
        # - le parcours est fait en largeur d'abord
        # - dès qu'une contradiction est identifiée, tous les Raisonnements qui
        # portent sur la même Proposition sont interrompus.
        retour = list()
        contradictions = set()

        # Initialisation : il y a autant de Raisonnements que de Propositions
        raisonnements = [Raisonnement([n]) for n in self.nœuds.values()]

        # Poursuite jusqu'à extinction des possibilités
        while len(raisonnements) != 0:
            nouveaux = list()
            lg_contrad = len(contradictions)
            for r in raisonnements:
                nœud = r[-1]
                if len(r) % 2 == 0:
                    for voisin in nœud.durs:
                        suite = r.développer(voisin)
                        if not (suite is None or suite.est_bouclé()):
                            nouveaux.append(suite)
                else:
                    for voisin in nœud.liens:
                        suite = r.développer(voisin)
                        if suite is not None:
                            if suite.est_contradictoire():
                                retour.append(suite)
                                contradictions.add(voisin)
                            elif not suite.est_bouclé():
                                nouveaux.append(suite)

            # Interruptions des Raisonnements
            if len(contradictions) != lg_contrad:
                for p in contradictions:
                    i = 0
                    lg = len(nouveaux)
                    while i < lg:
                        r = nouveaux[i]
                        if r[0] is p:
                            nouveaux[i] = nouveaux[lg - 1]
                            lg -= 1
                        else:
                            i += 1
                    del nouveaux[lg:]

            # Itération
            raisonnements = nouveaux

        return retour


class Traitement:

    def __init__(self, conf):
        self.lots = conf.lots
        self.codec = None

    def effectuer(self):
        for lot in self.lots:
            self.codec = Codec.singleton(lot.base)
            for code in lot:
                self.gérer_grille(code)
            self.codec = None

    def gérer_grille(self, code):
        grille = self.codec.décoder(code)

        logging.info("Grille à résoudre :\n" + str(grille))

        # Maintenant, on cherche à résoudre
        self.résoudre(grille)

    def résoudre(self, grille):
        # Modification du modèle Entité-Association
        grille = GrilleNavigable(grille)

        # On applique itérativement les techniques
        difficulté = -1
        techniques = [
            self.technique_1_1,
            self.technique_1_2,
            self.technique_1_3,
            self.technique_2_1,
            self.technique_2_2,
            self.technique_2_3,
            self.technique_3_1,
            # self.technique_3_2,
            # self.technique_3_3,
            self.technique_3_4,
            # self.technique_3_5,
            # self.technique_3_6,
            self.technique_4_1,
            self.technique_4_3,
            self.technique_4_4,
        ]
        modification = True
        while modification:
            modification = False
            for i, t in enumerate(techniques):
                if t(grille):
                    modification = True
                    if i > difficulté:
                        difficulté = i
                    break
            if grille.est_complète():
                break

        if grille.est_complète():
            logging.info(f"Grille résolue :\n{grille}")
            logging.info(
                f"Difficulté estimée : {difficulté} / {len(techniques)-1}")
        else:
            logging.info(f"Grille non résolue :\n{grille}")
            difficulté = None

        return difficulté

    def technique_1_1(self, grille):
        """Full block

        Dans un bloc donné, il ne reste qu’une seule case non résolue.

        https://sudoku.megastar.fr/full-block/
        """
        retour = False
        for r in grille.régions.values():
            cases = [c for c in r.cases if c.libres is not None]
            if len(cases) == 1:
                retour = True
                c = cases[0]
                c.fixer()
                logging.info(f"Technique 1.1 : {c.position} = {c.valeur}")
        return retour

    def technique_1_2(self, grille):
        """Unique

        Dans un bloc donné, une seule case propose une valeur absente
        ailleurs.

        https://sudoku.megastar.fr/unique/
        """
        retour = False
        for r in grille.régions.values():
            libres = r.libres()
            for l in libres:
                cases = [
                    c for c in r.cases
                    if c.libres is not None and l in c.libres
                ]
                if len(cases) == 1:
                    retour = True
                    c = cases[0]
                    c.fixer(l)
                    logging.info(f"Technique 1.2 : {c.position} = {c.valeur}")
        return retour

    def technique_1_3(self, grille):
        """Crown

        Tous les voisins ont une valeur, sauf une.

        https://sudoku.megastar.fr/crown/
        """
        retour = False
        for c in grille.cases:
            if c.libres is not None and len(c.libres) == 1:
                retour = True
                c.fixer()
                logging.info(f"Technique 1.3 : {c.position} = {c.valeur}")
        return retour

    def technique_2_1(self, grille):
        """Simple-round N (2,3,4)

        Un candidat à l’extérieur d’un bloc peut voir toutes ses N
        occurrences à l’intérieur du bloc.

        https://sudoku.megastar.fr/simple-round-2/
        https://sudoku.megastar.fr/simple-round-3/
        https://sudoku.megastar.fr/simple-round-4/
        """
        retour = False
        for r in grille.régions.values():
            libres = r.libres()
            for l in libres:
                cases = [
                    c for c in r.cases
                    if c.libres is not None and l in c.libres
                ]
                # Identification des voisins communs à toutes les cases
                voisins = None
                for c in cases:
                    if voisins is None:
                        voisins = set(c.voisins)
                    else:
                        voisins.intersection_update(c.voisins)

                for c in voisins:
                    if c.libres is not None and l in c.libres:
                        retour = True
                        c.libres.discard(l)
                        logging.info(
                            f"Technique 2.1 : {c.position} → retrait de {l}")
        return retour

    def technique_2_2(self, grille):
        """Simple naked pair

        S’il existe deux cases ayant seulement deux candidats identiques,
        on peut éliminer ces candidats dans toutes les autres cases qui
        voient ces cases.

        https://sudoku.megastar.fr/simple-naked-pair/
        """
        retour = False
        for r in grille.régions.values():
            cases = [
                c for c in r.cases
                if c.libres is not None and len(c.libres) == 2
            ]
            for a, b in itertools.combinations(cases, 2):
                if a.libres == b.libres:
                    for c in r.cases:
                        if (c.libres is not None and c is not a
                                and c is not b):
                            for l in a.libres:
                                if l in c.libres:
                                    retour = True
                                    c.libres.discard(l)
                                    logging.info(
                                        f"Technique 2.2 : {c.position} → retrait de {l}"
                                    )
        return retour

    def technique_2_3(self, grille):
        """Simple hidden pair

        Si deux cases d’un bloc ont deux candidats identiques qui
        n’apparaissent nulle part ailleurs dans les autres cases du bloc,
        alors on peut éliminer tous les autres candidats des cases
        contenant ces deux candidats.

        https://sudoku.megastar.fr/simple-hidden-pair/
        """
        retour = False
        for r in grille.régions.values():
            cases = [
                c for c in r.cases
                if c.libres is not None and len(c.libres) >= 2
            ]

            # Ensemble des positions possibles pour chaque valeur libre
            pos = dict()
            for l in r.libres():
                pos[l] = set([c.position for c in cases if l in c.libres])

            # Ensemble des valeurs présentes en 2 positions uniquement
            deux = [v for v in pos if len(pos[v]) == 2]

            for x, y in itertools.combinations(deux, 2):
                if pos[x] == pos[y]:
                    for p in pos[x]:
                        c = grille[p]
                        supprimées = set(c.libres).difference(set([x, y]))
                        for s in supprimées:
                            retour = True
                            c.libres.discard(s)
                            logging.info(
                                f"Technique 2.3 : {c.position} → retrait de {s}"
                            )

        return retour

    def technique_3_1(self, grille):
        """Naked single

        Il n'y a qu'une seule valeur possible pour une case.

        https://sudoku.megastar.fr/naked-single/
        """
        retour = False
        for c in grille.cases:
            if c.libres is not None and len(c.libres) == 1:
                retour = True
                c.fixer()
                logging.info(f"Technique 3.1 : {c.position} = {c.valeur}")
        return retour

    def technique_3_2(self, grille):
        """Hidden single

        Dans une région, si une valeur ne peut être accueillie que par une
        seule case, alors elle est portée par celle-ci.

        https://sudoku.megastar.fr/hidden-single/
        """
        return self.technique_1_2(grille)

    def technique_3_3(self, grille):
        """Hard round N (2, 3, 4)

        Idem simple round N

        https://sudoku.megastar.fr/hard-round-2/
        https://sudoku.megastar.fr/hard-round-3/
        https://sudoku.megastar.fr/hard-round-4/
        """
        return self.technique_2_1(grille)

    def technique_3_4(self, grille):
        """Naked pair B

        Deux cases voisines l'une de l'autre qui offrent la même paire de
        valeur empêchent leurs voisines communes d'offrir ces valeurs.

        https://sudoku.megastar.fr/naked-pair-b/
        """
        retour = False
        candidates = [c for c in grille.cases if c.libres is not None]

        # Puisque on cherche un triplet de cases voisines deux à deux, on
        # modifie le point de vue de l'algorithme.
        # Normalement, il faudrait trouver les voisins communs à deux cases
        # voisines, et s'intéresser à ces voisins.
        # Ici, on regarde si deux cases voisines à une même troisième le sont
        # entre elles, et on regarde leur influence sur la première.

        for c in candidates:
            voisines = [
                v for v in c.voisins
                if v.libres is not None and len(v.libres) == 2
            ]
            for v in voisines:
                communes = [w for w in voisines if w in v.voisins]
                for w in communes:
                    if v.libres == w.libres:
                        for s in v.libres:
                            if s in c.libres:
                                retour = True
                                c.libres.discard(s)
                                logging.info(
                                    f"Technique 3.4 : {c.position} → retrait de {s}"
                                )
        return retour

    def technique_3_5(self, grille):
        """Hard naked pair

        Idem «simple naked pair»

        https://sudoku.megastar.fr/hard-naked-pair/
        """
        return self.technique_2_2(grille)

    def technique_3_6(self, grille):
        """Hard hidden pair

        Idem «simple hidden pair»

        https://sudoku.megastar.fr/hard-hidden-pair/
        """
        return self.technique_2_3(grille)

    def technique_4_1(self, grille):
        """Same value cells

        L'agencement des cases fait que deux cases de deux régions voisines
        prennent mécaniquement la même valeur. Elles offrent donc les mêmes
        candidats.

        https://sudoku.megastar.fr/same-value-cells/
        """
        retour = False
        for r in grille.régions.values():
            for c in r.cases:
                if c.libres is not None:
                    # On identifie les voisins communs à toutes les autres
                    # cases de la région
                    communs = None
                    for a in r.cases:
                        if a is not c:
                            if communs is None:
                                communs = set(a.voisins)
                            else:
                                communs.intersection_update(set(a.voisins))
                    # On écarte la case elle-même, ainsi que ses propres
                    # voisines
                    communs.discard(c)
                    communs.difference_update(set(c.voisins))

                    #
                    if len(communs) != 0:
                        for s in communs:
                            if s.libres is None:
                                if s.valeur in c.libres:
                                    retour = True
                                    logging.info(
                                        f"Technique 4.1 : {c.position} = {s.valeur}"
                                    )
                                    c.fixer(s.valeur)
                            elif c.libres.issuperset(s.libres) and len(
                                    c.libres) > len(s.libres):
                                valeurs = c.libres.difference(s.libres)
                                for l in valeurs:
                                    retour = True
                                    logging.info(
                                        f"Technique 4.1 : {c.position} →"
                                        f" retrait de {l}")
                                    c.libres.remove(l)

        return retour

    def technique_4_3(self, grille):
        """X-chain

        Cette technique cherche à identifier des contradictions avec des
        Raisonnements ne portant que sur une unique valeur.

        https://sudoku.megastar.fr/x-chain/
        """
        retour = False

        # Graphes par valeurs
        graphes = dict()

        # Identification des liens forts sur valeur unique
        for r in grille.régions.values():
            libres = dict()
            for c in r.cases:
                if c.libres is not None:
                    for l in c.libres:
                        libres.setdefault(l, list()).append(Proposition(c, l))
            for l, propositions in libres.items():
                if len(propositions) == 2:
                    graphes.setdefault(
                        l, Logigraphe()).créer_lien_dur(*propositions)
                else:
                    for p1, p2 in itertools.combinations(propositions, 2):
                        graphes.setdefault(l, Logigraphe()).créer_lien_mou(
                            p1, p2)

        # Identification des liens faibles sur valeur unique
        faibles = dict()
        for c in grille.cases:
            if c.libres is not None:
                for v in c.voisins:
                    if (c.région != v.région and v.libres is not None):
                        communs = c.libres.intersection(v.libres)
                        for l in communs:
                            faibles.setdefault(l, list()).append((c, v))
                            if (v, c) in faibles[l]:
                                del faibles[l][-1]
        for l in faibles:
            graphe = graphes.setdefault(l, Logigraphe())
            for c1, c2 in faibles[l]:
                graphe.créer_lien_mou(Proposition(c1, l), Proposition(c2, l))

        # Contradictions
        for g in graphes.values():
            contradictions = g.identifier_contradictions()
            for r in contradictions:
                logging.debug(r)

                retour = True
                proposition = r[0].proposition
                proposition.case.libres.discard(proposition.valeur)
                logging.info(
                    f"Technique 4.3 : {proposition.case.position} → retrait de"
                    f" {proposition.valeur}")

        return retour

    def technique_4_4(self, grille):
        """XY-chain

        Cette technique cherche à identifier des contradictions avec des
        Raisonnements portant sur tout type de valeur.

        https://sudoku.megastar.fr/xy-chain/
        """
        retour = False

        # À la différence de X-chaîne, on ne maintient qu'un seul graphe
        graphe = Logigraphe()

        logging.debug(grille)

        # Liens portant sur la case elle-même
        for c in grille.cases:
            if c.libres is not None:
                propositions = [Proposition(c, l) for l in c.libres]
                if len(propositions) == 2:
                    graphe.créer_lien_dur(*propositions)
                else:
                    for p1, p2 in itertools.combinations(propositions, 2):
                        graphe.créer_lien_mou(p1, p2)

        # Identification des liens sur valeur unique au sein d'une région
        for r in grille.régions.values():
            libres = dict()
            for c in r.cases:
                if c.libres is not None:
                    for l in c.libres:
                        libres.setdefault(l, list()).append(c)
            for l, cases in libres.items():
                propositions = [Proposition(c, l) for c in cases]
                if len(propositions) == 2:
                    graphe.créer_lien_dur(*propositions)
                else:
                    for p1, p2 in itertools.combinations(propositions, 2):
                        graphe.créer_lien_mou(p1, p2)

        # Identification des liens faibles sur valeur unique
        faibles = dict()
        for c in grille.cases:
            if c.libres is not None:
                for v in c.voisins:
                    if (c.région != v.région and v.libres is not None):
                        communs = c.libres.intersection(v.libres)
                        for l in communs:
                            faibles.setdefault(l, list()).append((c, v))
                            if (v, c) in faibles[l]:
                                del faibles[l][-1]
        for l in faibles:
            for c1, c2 in faibles[l]:
                graphe.créer_lien_mou(Proposition(c1, l), Proposition(c2, l))

        # Sélection des raisonnements les plus courts amenant à une
        # contradiction
        contradictions = graphe.identifier_contradictions()
        i = 0
        lg = len(contradictions)
        while i < lg:
            j = i + 1
            while j < lg:
                if contradictions[i][0].proposition == contradictions[j][
                        0].proposition:
                    if len(contradictions[i]) > len(contradictions[j]):
                        contradictions[i] = contradictions[j]
                    lg -= 1
                    contradictions[j] = contradictions[lg]
                else:
                    j += 1
            i += 1
        del contradictions[lg:]

        for r in contradictions:
            logging.debug(r)
            retour = True
            proposition = r[0].proposition
            proposition.case.libres.discard(proposition.valeur)
            logging.info(
                f"Technique 4.4 : {proposition.case.position} → retrait de {proposition.valeur}"
            )

        return retour


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    CONF = Configuration.charger()
    T = Traitement(CONF)
    T.effectuer()
