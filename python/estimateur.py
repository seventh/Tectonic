#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Détermine le nombre de bits réellement consommés par des lots
"""

import logging

from commun import Configuration


def estimer(lot):
    # Cartographie préliminaire
    codes_par_nb_bits = dict()
    codes_par_nb_octets = dict()
    for code in lot:
        nb_bits = code.bit_length()
        nombre = codes_par_nb_bits.get(nb_bits, 0)
        codes_par_nb_bits[nb_bits] = nombre + 1

        nb_octets = (nb_bits + 7) // 8
        nombre = codes_par_nb_octets.get(nb_octets, 0)
        codes_par_nb_octets[nb_octets] = nombre + 1

    # Estimation de la taille précédente
    avant = 22 + 5 * len(codes_par_nb_octets)
    for nb_octets, nb_codes in codes_par_nb_octets.items():
        avant += nb_octets * nb_codes

    # Estimation à proprement parler
    # Si les codes sont organisés par blocs de `n` bits, alors le bit de
    # poids fort peut lui-même être ignoré, puisque par définition, il vaut
    # 1.

    après = 22 + 5 * len(codes_par_nb_bits)
    for nb_bits, nb_codes in codes_par_nb_bits.items():
        if nb_bits != 0:
            nb_bits_bloc = (nb_bits - 1) * nb_codes
            nb_octets_blocs = (nb_bits_bloc + 7) // 8
            après += nb_octets_blocs

    # Affichage de la sortie
    logging.info(f"{avant} → {après}, soit {après/avant-1:.2%}")


if __name__ == "__main__":
    CONF = Configuration.charger()
    if CONF.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    for LOT in CONF.lots:
        estimer(LOT)
