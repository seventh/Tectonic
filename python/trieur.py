#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from commun import Configuration

from tectonic.fichier_001 import Écrivain
from tectonic.fichier_001 import Rangement


def trier(config, lot):
    codes = dict()
    écrivain = Écrivain("toto1.out", lot.base, rangement=Rangement.NORMAL)
    for code in lot:
        nb_bits = code.bit_length()
        taille = (nb_bits + 7) // 8
        codes.setdefault(taille, list()).append(code)
        écrivain.ajouter(code)
    écrivain.clore()

    écrivain = Écrivain("toto2.out", lot.base, rangement=Rangement.DIRECT)
    for taille in sorted(codes):
        for code in sorted(codes[taille]):
            écrivain.ajouter(code)
    écrivain.clore()


if __name__ == "__main__":
    CONF = Configuration.charger()
    for LOT in CONF.lots:
        trier(CONF, LOT)
