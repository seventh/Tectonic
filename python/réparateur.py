#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Répare un fichier au format TECTONIC1

ATTENTION : ce script a avant tout servi à passer le champ de l'en-tête du
nombre de codes de 4 à 8 octets. Il n'est en l'état pas mûr pour réparer un
fichier au format TECTONIC1 «nouvelle mouture»
"""

from collections import namedtuple
import logging
import os
import struct

from commun import Configuration

Segment = namedtuple("Segment", "taille nombre")


class Bloc:

    def __init__(self, entrée):
        self.décalage = entrée.tell()
        self.segments = list()
        n = int.from_bytes(entrée.read(1), byteorder="big", signed=True)
        while n > 0:
            taille = int.from_bytes(entrée.read(1),
                                    byteorder="big",
                                    signed=False)
            nombre = int.from_bytes(entrée.read(4),
                                    byteorder="big",
                                    signed=False)
            self.segments.append(Segment(taille, nombre))
            n -= 1
        self.contenu = entrée.tell()

    def __str__(self):
        lignes = list()
        lignes.append("Bloc")
        lignes.append(
            f"  - en-tête : [0x{self.décalage:08x} 0x{self.contenu:08x}]")
        début = self.contenu
        for i, s in enumerate(self.segments):
            fin = début + s.nombre * s.taille
            lignes.append(
                f"  - bloc {i} : {s.nombre:8d} codes × {s.taille} octets : [0x{début:08x} 0x{fin:08x}]"
            )
            début = fin
        return "\n".join(lignes)

    def fin(self):
        retour = self.contenu
        for s in self.segments:
            retour += s.nombre * s.taille
        return retour

    def tailles(self):
        retour = sorted(set([s.taille for s in self.segments]))
        return retour

    def sélection(self, taille):
        """Itérateur des segments de la taille demandée

        Chaque réponse est de la forme (décalage, nombre)
        """
        décalage = self.contenu
        for s in self.segments:
            if s.taille == taille:
                yield (décalage, s.nombre)
            décalage += s.nombre * s.taille


def réparer(lecteur, suffixe):
    chemin = lecteur.chemin

    # Cartographie pour vérifier la complétude du fichier
    blocs = list()
    with open(chemin, "rb") as entrée:
        entrée.seek(16, os.SEEK_SET)
        while True:
            b = Bloc(entrée)
            if len(b.segments) == 0:
                break
            blocs.append(b)
            entrée.seek(b.fin(), os.SEEK_SET)

    nb_codes = 0
    for b in blocs:
        logging.debug(b)
        for s in b.segments:
            nb_codes += s.nombre

    logging.info(f"{nb_codes} codes")

    # Réparation
    with (open(chemin, "rb") as entrée, open(chemin + suffixe, "wb") as
          sortie):
        # En-tête
        sortie.write(entrée.read(12))

        # - Nombre de codes
        entrée.seek(4, os.SEEK_CUR)
        sortie.write(struct.pack(">Q", nb_codes))

        # Contenu
        while True:
            morceau = entrée.read(1024 * 1024 * 4)
            if len(morceau) == 0:
                break
            sortie.write(morceau)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    CONF = Configuration.charger()
    for lot in CONF.lots:
        réparer(lot, CONF.suffixe)
