#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Générateur de grille complète

À la différence du générateur original, celui-ci adopte une approche en
profondeur d'abord.

De plus, l'interruption/reprise de la génération est prise en compte,
sans recourir à un fichier intermédiaire, tout simplement en repartant
de la dernière grille générée.
"""

import dataclasses
import getopt
import logging
import os.path
import sys
import signal

from tectonic import Base
from tectonic import Progrès
from tectonic.fichier import écrivain as get_écrivain
from tectonic.fichier import lecteur as get_lecteur
from générateur import GénérateurGrille

ARRÊT_DEMANDÉ = False


def gestionnaire(signum, pile):
    """Interception de signaux particuliers :

    - SIGHUP (mort du processus père)
    - SIGINT (demande d'interruption)
    """
    global ARRÊT_DEMANDÉ

    if signum in [signal.SIGHUP, signal.SIGINT, signal.SIGTERM]:
        logging.info("Interruption des recherches demandée…")
        ARRÊT_DEMANDÉ = True


@dataclasses.dataclass
class Configuration:

    hauteur: int = 5
    largeur: int = 4
    maximum: int = 5
    chemin: str = "../data"

    @staticmethod
    def charger():
        retour = Configuration()
        opts, args = getopt.getopt(sys.argv[1:], "h:l:m:")
        for opt, val in opts:
            if not val.isdecimal():
                logging.warning(f"Option «{opt} {val}» ignorée")
                continue
            else:
                val = int(val)
                if opt == "-h" and val > 0:
                    retour.hauteur = val
                elif opt == "-l" and val > 0:
                    retour.largeur = val
                elif opt == "-m" and val > 2:
                    retour.maximum = val
        if len(args) != 0:
            retour.chemin = args[0]

        return retour

    def base(self):
        return Base.singleton(hauteur=self.hauteur,
                              largeur=self.largeur,
                              maximum=self.maximum)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    # Pour capturer une demande d'arrêt
    signal.signal(signal.SIGHUP, gestionnaire)
    signal.signal(signal.SIGINT, gestionnaire)
    signal.signal(signal.SIGTERM, gestionnaire)

    CONF = Configuration.charger()
    logging.info(CONF)

    CHEMIN = os.path.join(
        CONF.chemin,
        str(Progrès(CONF.hauteur, CONF.largeur, CONF.maximum)) + ".log")

    # Mise-en-place du générateur
    CHERCHEUR = GénérateurGrille(CONF.base())

    # Reprise
    if not os.path.exists(CHEMIN):
        iter(CHERCHEUR)
    else:
        logging.info("Reprise…")
        LECTEUR = get_lecteur(CHEMIN)
        LECTEUR.seek(-1, os.SEEK_END)
        CODE = next(LECTEUR)
        logging.debug(CODE)
        CHERCHEUR.retrouver_chemin(CODE)
        logging.info("Reprise OK")

    ENREGISTREUR = get_écrivain(CHEMIN,
                                CONF.base(),
                                reprise=True,
                                progressif=True)

    # Exploration
    while not ARRÊT_DEMANDÉ:
        try:
            CODE = next(CHERCHEUR)
            logging.debug(CODE)
            ENREGISTREUR.ajouter(CODE)
        except StopIteration:
            break

    # Terminaison
    ENREGISTREUR.clore()
