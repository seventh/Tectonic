# -*- coding: utf-8 -*-
"""Code commun à la génération de grilles pleines
"""

import dataclasses
import itertools
import logging
import random

from tectonic import Grille
from tectonic import Progrès
from tectonic.serial import Codec


@dataclasses.dataclass
class Région:
    """Ensemble de valeurs connexes et nombre de bords libres
    """

    # Valeurs des cases de la région
    valeurs: set = dataclasses.field(default_factory=set)
    # Identifiants des régions voisines
    voisins: set = dataclasses.field(default_factory=set)
    # Nombre de cases limitrophes libres
    bordure: int = 0

    def __eq__(self, autre):
        return (self.valeurs == autre.valeurs and self.voisins == autre.voisins
                and self.bordure == autre.bordure)

    def est_anormal(self):
        """Une Zone est anormale si elle est fermée et ne contient pas toutes
        les valeurs prévues
        """
        retour = False
        if self.bordure == 0 and self.est_incomplète():
            retour = True
        return retour

    def est_incomplète(self):
        """Vrai ssi les valeurs ne forment pas un intervalle continu
        débutant à 1. Toutes les valeurs autorisées n'ont pas à être
        représentées (dans une grille autorisant le 5, une région avec les
        quatre valeurs 1, 2, 3, 4 sera considérée complète)
        """
        n = len(self.valeurs)
        if n == 0:
            return True

        minimum = min(self.valeurs)
        if minimum != 1:
            return True

        maximum = max(self.valeurs)
        if n != maximum - minimum + 1:
            return True

        return False


class Analyseur:
    """Analyse d'une grille

    Cette analyse consiste à identifier, région par région, les valeurs dont
    elles sont constituées, ainsi que les identifiants des régions connexes,
    et le nombre de cases limitrophes libres.
    """

    def __init__(self, grille):
        self.g = grille
        self.régions = dict()

        self._calculer()

    def _calculer(self):
        bords = dict()

        for i, case in enumerate(self.g.cases):
            r1 = case.région
            if r1 >= 0:
                if case.valeur > 0:
                    self.régions.setdefault(r1,
                                            Région()).valeurs.add(case.valeur)

                h1, l1 = self.g.base.en_position(i)
                for h2, l2 in [(h1, l1 + 1), (h1 + 1, l1)]:
                    if (h2 < self.g.base.hauteur and l2 < self.g.base.largeur):
                        j = self.g.base.en_index(hauteur=h2, largeur=l2)
                        r2 = self.g.cases[j].région
                        if r2 < 0:
                            bords.setdefault(r1, set()).add(j)
                        elif r2 != r1:
                            self.régions.setdefault(r1,
                                                    Région()).voisins.add(r2)
                            self.régions.setdefault(r2,
                                                    Région()).voisins.add(r1)

        for r, cases in bords.items():
            self.régions[r].bordure = len(cases)

    def région_max(self):
        return max(self.régions, default=-1)


class GénérateurGrilleVide:

    def __init__(self, base):
        grille = Grille(base)
        self.code = Codec.singleton(base).encoder(grille)

        self.a_produit = True

    def __iter__(self):
        self.a_produit = False
        return self

    def __next__(self):
        if self.a_produit:
            raise StopIteration
        else:
            self.a_produit = True
            return self.code


class ProducteurProgrès:
    """Produit des grilles *pour* un certain palier, à partir d'un code issu
    du palier précédent
    """

    def __init__(self, progrès):
        self.palier = progrès.palier
        self.codec = Codec.singleton(progrès.base)

    def itérer(self, code):
        """Itérateur des grilles du palier suivant

        Attention, pour le dernier palier, il faut vérifier la validité des
        grilles générées !
        """
        grille = self.codec.décoder(code)
        analyseur = Analyseur(grille)

        i = self.palier - 1
        h, l = grille.base.en_position(i)

        # Régions à compléter
        régions = set()
        # → case du dessus
        if h > 0:
            régions.add(grille[(h - 1, l)].région)
        # → case de gauche
        if l > 0:
            régions.add(grille[(h, l - 1)].région)
        régions = sorted(régions)

        # Calcul des valeurs possibles au maximum, selon la règle du voisinage
        valeurs_possibles = set(range(1, grille.base.maximum + 1))
        if l > 0:
            valeurs_possibles.discard(grille[(h, l - 1)].valeur)
        if h > 0:
            valeurs_possibles.discard(grille[(h - 1, l)].valeur)
            if l > 0:
                valeurs_possibles.discard(grille[(h - 1, l - 1)].valeur)
            if l + 1 < grille.base.largeur:
                valeurs_possibles.discard(grille[(h - 1, l + 1)].valeur)

        # 1) On étend chacune des régions de toutes les façons possibles
        if len(régions) == 1:
            r = régions[0]
            valeurs = valeurs_possibles.difference(
                analyseur.régions[r].valeurs)
            for v in valeurs:
                grille.cases[i].région = r
                grille.cases[i].valeur = v
                yield self.codec.encoder(grille)
        else:
            for r1, r2 in itertools.permutations(régions, 2):
                # On vérifie qu'on ne rendrait pas «r2» incomplète de toute
                # pièce
                if not (analyseur.régions[r2].bordure == 1
                        and analyseur.régions[r2].est_incomplète()):
                    valeurs = valeurs_possibles.difference(
                        analyseur.régions[r1].valeurs)
                    for v in valeurs:
                        grille.cases[i].région = r1
                        grille.cases[i].valeur = v
                        yield self.codec.encoder(grille)

        # 2) On crée une toute nouvelle région, en veillant à ce qu'elle ne
        # puisse pas être incomplète
        for r in régions:
            if (analyseur.régions[r].bordure == 1
                    and analyseur.régions[r].est_incomplète()):
                break
        else:
            for v in valeurs_possibles:
                grille.cases[i].région = analyseur.région_max() + 1
                grille.cases[i].valeur = v
                yield self.codec.encoder(grille)

        # 3) On fusionne les régions voisines
        for r1, r2 in itertools.combinations(régions, 2):
            if (r2 not in analyseur.régions[r1].voisins
                    and analyseur.régions[r1].valeurs.isdisjoint(
                        analyseur.régions[r2].valeurs)):
                valeurs = valeurs_possibles.difference(
                    analyseur.régions[r1].valeurs)
                valeurs.difference_update(analyseur.régions[r2].valeurs)
                if len(valeurs) > 0:
                    for case in grille.cases:
                        if case.région == r2:
                            case.région = r1
                    grille.cases[i].région = r1
                    grille.normaliser()
                    for v in valeurs:
                        grille.cases[i].valeur = v
                        yield self.codec.encoder(grille)


class GénérateurPalier:
    """Foncteur de génération de grille incomplète.

    Dérive toutes les Grilles de palier `p` à partir d'une Grille de
    palier `p-1`, de telle façon qu'une Grille de palier `p` n'est issue
    de la dérivation que d'une seule Grille de palier `p-1`.
    """

    def __init__(self, progrès):
        self.progrès = progrès
        self.codec = Codec.singleton(progrès.base)

    def __call__(self, code):
        """Codes des grilles filles d'un code d'une grille de palier-1.

        Au contraire de ProducteurProgrès, toutes les grilles générées
        sont valides.

        `code` est ignoré pour la génération du palier 0.
        """
        if self.progrès.palier == 0:
            return list(GénérateurGrilleVide(self.progrès.base))
        elif self.progrès.est_intermédiaire():
            return list(ProducteurProgrès(self.progrès).itérer(code))
        else:
            retour = list()
            for code in ProducteurProgrès(self.progrès).itérer(code):
                grille = self.codec.décoder(code)
                analyseur = Analyseur(grille)
                for r in analyseur.régions.values():
                    if r.est_anormal():
                        break
                else:
                    retour.append(code)
            return retour


class GénérateurGrille:
    """Itérateur de génération de grille complète
    """

    def __init__(self, base):
        self.base = base
        self.nb_cases = self.base.nb_cases()

        self.chemin = list()
        self.filles = list()
        self.gen = list()
        for palier in range(self.nb_cases + 1):
            progrès = Progrès(hauteur=base.hauteur,
                              largeur=base.largeur,
                              maximum=base.maximum,
                              palier=palier)
            self.gen.append(GénérateurPalier(progrès))

    def __iter__(self):
        # Initialisation au tout début de l'itération
        self.chemin[:] = [0]
        self.filles[:] = [self.gen[0](None)]
        return self

    def __next__(self):
        p = len(self.chemin) - 1

        if p == self.nb_cases:
            self.chemin[p] += 1

        while p >= 0:
            if self.chemin[p] == len(self.filles[p]):
                self.chemin.pop()
                self.filles.pop()
                p -= 1
                if p >= 0:
                    self.chemin[p] += 1

            else:
                code = self.filles[p][self.chemin[p]]
                if p == self.nb_cases:
                    logging.debug(self.chemin)
                    return code
                else:
                    p += 1
                    self.chemin.append(0)
                    self.filles.append(self.gen[p](code))

        raise StopIteration

    def lit_chemin(self):
        return self.chemin

    def met_chemin(self, chemin):
        assert len(chemin) <= self.nb_cases + 1
        code = None
        self.chemin.clear()
        self.filles[:] = [self.gen[0](code)]
        for p, étape in enumerate(chemin):
            # Pour garantir le bon fonctionnement ultérieur de l'itérateur, on
            # s'assure ici du respect du domaine de valeur de chaque étape.
            if étape >= len(self.filles[p]):
                self.chemin.append(len(self.filles[p]))
                break
            elif étape < 0:
                étape = 0
            self.chemin.append(étape)
            code = self.filles[p][étape]
            if p < self.nb_cases:
                self.filles.append(self.gen[p + 1](code))

    def retrouver_chemin(self, code):
        """Positionne le générateur sur la production du code

        Fonctionne également avec un code de grille partielle
        """
        # Valeurs intéressantes pour les tests :
        #  base = (3,3,6) | code = 2208268805144068
        #  base = (11,9,5) | code = 1184585917684250729780227816225955399939854064068905289844072541609854648518388182012392789100351570060994960653989480226240091620268318683055448051742754661856020801908828388715101309284815136932762918191299043362584
        codec = Codec.singleton(self.base)
        grille = codec.décoder(code)
        logging.debug(grille)

        # Initialisation de la recherche
        self.__iter__()
        code = self.filles[-1][self.chemin[-1]]

        # Recherche itérative
        for i in range(self.nb_cases):
            if grille.cases[i].valeur == -1:
                break

            p = i + 1
            self.filles.append(self.gen[p](code))

            # Index des cases voisines
            h, l = grille.base.en_position(i)
            if h > 0:
                u = grille.base.en_index(hauteur=h - 1, largeur=l)
            if l > 0:
                v = grille.base.en_index(hauteur=h, largeur=l - 1)

            # Sélection en 2 temps
            éligibles = list()
            for k, fille in enumerate(self.filles[-1]):
                candidate = codec.décoder(fille)
                if (candidate.cases[i].valeur == grille.cases[i].valeur and
                    (h == 0 or
                     (grille.cases[u].région == grille.cases[i].région) ==
                     (candidate.cases[u].région == candidate.cases[i].région))
                        and (l == 0 or
                             (grille.cases[v].région == grille.cases[i].région)
                             == (candidate.cases[v].région
                                 == candidate.cases[i].région))):
                    éligibles.append(k)
            logging.debug(éligibles)
            if len(éligibles) != 1:
                logging.error("Impossible à retracer !")
                break
            else:
                k = éligibles[0]
                self.chemin.append(k)
                code = self.filles[-1][k]

    def tirer_chemin(self):
        """Proposer un chemin aléatoire
        """
        self.__iter__()

        for p in range(1, self.nb_cases + 1):
            if self.chemin[-1] == len(self.filles[-1]):
                break
            code = self.filles[-1][self.chemin[-1]]
            self.filles.append(self.gen[p](code))
            self.chemin.append(random.randrange(len(self.filles[p]) + 1))
        logging.debug(self.chemin)
