#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Compacteur de fichiers au format TECTONIC1

Minimise le nombre de blocs. Idéalement, un seul. L'outil s'appuie sur une
connaissance fine de la structure de données pour effectuer des traitements
par lot, plutôt que code par code.
"""

import logging
import os

from commun import Configuration
from tectonic.fichier_001 import analyser
from tectonic.fichier_001 import entier_en_binaire


def compacter(lecteur, suffixe):
    chemin = lecteur.chemin

    # Cartographie
    with open(chemin, "rb") as entrée:
        segments = analyser(entrée)
        for s in segments:
            logging.debug(s)
            logging.debug(f"→ {s.limite()}")

    # Caractéristiques du bloc compact
    nombre_par_taille = dict()
    for s in segments:
        n = nombre_par_taille.get(s.taille, 0) + s.nombre
        nombre_par_taille[s.taille] = n
    tailles = sorted(nombre_par_taille)

    # Compactage
    with open(chemin + suffixe, "wb") as sortie:
        with open(chemin, "rb") as entrée:
            # En-tête global
            sortie.write(entrée.read(20))

            # Blocs
            while len(tailles) != 0:
                nb_segments = min(len(tailles), 255)

                # En-tête de bloc
                sortie.write(entier_en_binaire(1, nb_segments))
                for taille in tailles[:nb_segments]:
                    sortie.write(entier_en_binaire(1, taille))
                    sortie.write(
                        entier_en_binaire(4, nombre_par_taille[taille]))

                # Bloc
                for taille in tailles[:nb_segments]:
                    for s in segments:
                        if s.taille == taille:
                            entrée.seek(s.avance, os.SEEK_SET)
                            sortie.write(entrée.read(s.volume()))

                del tailles[:nb_segments]

            # Marque finale
            sortie.write(entier_en_binaire(1, 0))


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    CONF = Configuration.charger()
    for LOT in CONF.lots:
        logging.info(f"Traitement de {LOT.chemin}")
        compacter(LOT, CONF.suffixe)
