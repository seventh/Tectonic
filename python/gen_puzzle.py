#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Générateur de puzzle

Le générateur fonctionne en deux étapes :
1) Génération d'une grille valide
2) Décimation de valeurs dans cette grille jusqu'à atteindre la
difficulté de résolution souhaitée
"""

import dataclasses
import getopt
import logging
import sys
import random

from générateur import GénérateurGrille
from solveur import Traitement as Solveur
from commun import Configuration as SolveurConfiguration
from tectonic import Base
from tectonic.serial import Codec


@dataclasses.dataclass
class Configuration:

    debug: bool = False
    difficulté: int = 3
    hauteur: int = 5
    largeur: int = 4
    maximum: int = 5

    @staticmethod
    def charger():
        retour = Configuration()
        opts, args = getopt.getopt(sys.argv[1:], "d:gh:l:m:")
        for opt, val in opts:
            if opt == "-g":
                retour.debug = True
            elif not val.isdecimal():
                logging.warning(f"Option «{opt} {val}» ignorée")
                continue
            else:
                val = int(val)
                if opt == "-h" and val > 0:
                    retour.hauteur = val
                elif opt == "-l" and val > 0:
                    retour.largeur = val
                elif opt == "-m" and val > 2:
                    retour.maximum = val
        if len(args) != 0:
            logging.warning(f"Arguments {args} ignorés")

        return retour

    def base(self):
        return Base.singleton(hauteur=self.hauteur,
                              largeur=self.largeur,
                              maximum=self.maximum)


class Décimateur:
    """Créateur de puzzle d'une difficulté attendue
    """

    def __init__(self, base, difficulté):
        self.base = base
        self.difficulté = difficulté

    def trouver(self, code):
        codec = Codec(self.base)
        nb_essais_restants = 10
        trouvé = False

        solveur = Solveur(SolveurConfiguration())
        while not trouvé and nb_essais_restants > 0:
            grille = codec.décoder(code)
            while True:
                index = random.randrange(self.base.nb_cases())
                if grille.cases[index].valeur != -1:
                    grille.cases[index].valeur = -1
                    difficulté = solveur.résoudre(grille)
                    if difficulté is None:
                        # On a été trop loin : le puzzle ne peut plus être
                        # résolu
                        nb_essais_restants -= 1
                        break
                    elif difficulté >= self.difficulté:
                        trouvé = True
                        break

        if trouvé:
            logging.info("\n" + str(grille))

        return codec.encoder(grille)


if __name__ == "__main__":
    CONF = Configuration.charger()
    if CONF.debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    logging.info(CONF)

    random.seed(1977)

    # Phase n°1 : générer une grille aléatoire
    GEN = GénérateurGrille(CONF.base())
    TROUVÉ = False
    while not TROUVÉ:
        GEN.tirer_chemin()
        try:
            CODE = next(GEN)
            break
        except StopIteration:
            pass
    logging.info(f"Grille trouvée : {CODE}")

    # Phase n°2 : décimation
    DÉCIMATEUR = Décimateur(CONF.base(), CONF.difficulté)
    CODE = DÉCIMATEUR.trouver(CODE)
    logging.info(f"Puzzle proposé : {CODE}")
