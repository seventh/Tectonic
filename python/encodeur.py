#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

import tectonic
import tectonic.serial

if __name__ == "__main__":
    for NOM in sys.argv[1:]:
        # GRILLE = charger_grille(NOM)
        GRILLE = tectonic.Grille.depuis_chaîne(open(NOM, "rt").read())
        CODEC = tectonic.serial.Codec.singleton(GRILLE.base)
        CODE = CODEC.encoder(GRILLE)
        print(CODE)
