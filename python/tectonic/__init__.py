# -*- coding: utf-8 -*-
"""Éléments de base pour la manipulation de grilles de Tectonic
"""

import dataclasses
import re


class Base:
    """Caractéristiques principales d'une Grille

    Objet immuable : les attributs d'une instance ne doivent pas être
    modifiés.
    """

    # Singletons : dimensions → Base
    _instances = dict()

    @classmethod
    def singleton(cls, *, hauteur=5, largeur=4, maximum=5):
        """Instance unique associée aux dimensions correspondantes
        """
        neuf = Base(hauteur=hauteur, largeur=largeur, maximum=maximum)
        return Base._instances.setdefault(neuf, neuf)

    def __init__(self, *, hauteur=5, largeur=4, maximum=5):
        self.largeur = largeur
        self.hauteur = hauteur
        self.maximum = maximum

    def __eq__(self, autre):
        return (self.largeur == autre.largeur and self.hauteur == autre.hauteur
                and self.maximum == autre.maximum)

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        return (f"Base[0x{id(self):x}](largeur={self.largeur}, "
                f"hauteur={self.hauteur}, maximum={self.maximum})")

    def __str__(self):
        return (f"Base(largeur={self.largeur}, hauteur={self.hauteur}, "
                f"maximum={self.maximum})")

    def est_valide(self, *, hauteur, largeur):
        return (hauteur in range(0, self.hauteur)
                and largeur in range(0, self.largeur))

    def en_index(self, *, hauteur, largeur):
        """(hauteur, largeur) → index
        """
        return hauteur * self.largeur + largeur

    def en_position(self, index):
        """index → (hauteur, largeur)
        """
        return divmod(index, self.largeur)

    def nb_cases(self):
        return self.hauteur * self.largeur

    def transposée(self):
        """Objet dual dont hauteur et largeur ont été inversées
        """
        return Base.singleton(hauteur=self.largeur,
                              largeur=self.hauteur,
                              maximum=self.maximum)


class Progrès:
    """Progrès de recherche : base & palier atteints

    Le palier représente le nombre de cases renseignées.

    Objet immuable : les attributs d'une instance ne doivent pas être
    modifiés.
    """

    def __init__(self, hauteur, largeur, maximum, palier=None):
        """La base est obligatoire, le palier est optionnel

        Les arguments suivants sont acceptés :

        - `base` : pour spécifier directement la base.
        - `hauteur`, `largeur` et `maximum` : paramètres de la base, à
        défaut
        - `palier` : fixé à hauteur×largeur par défaut
        """
        self.base = Base.singleton(hauteur=hauteur,
                                   largeur=largeur,
                                   maximum=maximum)
        if palier is None:
            self.palier = hauteur * largeur
        else:
            self.palier = palier

    def __str__(self):
        retour = (f"h{self.base.hauteur:0>2}"
                  f"l{self.base.largeur:0>2}"
                  f"m{self.base.maximum:0>2}")
        if self.palier != self.base.hauteur * self.base.largeur:
            retour += f"-p{self.palier:0>2}"
        return retour

    def __repr__(self):
        return (f"{self.__class__.__name__}[0x{id(self):x}]"
                f"(hauteur={self.base.hauteur}"
                f",largeur={self.base.largeur}"
                f",maximum={self.base.maximum}"
                f",palier={self.palier})")

    _REGEX = re.compile("h(\\d+)l(\\d+)m(\\d+)(?:-p(\\d+))?")

    @staticmethod
    def depuis_chaîne(chaîne):
        retour = None
        m = Progrès._REGEX.search(chaîne)
        if m:
            retour = Progrès(*(int(x) for x in m.groups() if x is not None))
        return retour

    def est_intermédiaire(self):
        """Vrai ssi le palier n'est pas le dernier
        """
        return self.palier != self.base.hauteur * self.base.largeur

    def est_ultime(self):
        """Vrai ssi le palier est le dernier
        """
        return self.palier == self.base.hauteur * self.base.largeur

    @property
    def hauteur(self):
        return self.base.hauteur

    @property
    def largeur(self):
        return self.base.largeur

    @property
    def maximum(self):
        return self.base.maximum


@dataclasses.dataclass
class Case:

    valeur: int = -1
    région: int = -1


class Région:
    """Accesseur de Cases appartenant à une même Région
    """

    def __init__(self, grille, id, positions):
        self.grille = grille
        self.id = id
        self.positions = positions

        self._idx = 0

    def __len__(self):
        return len(self.positions)

    def __iter__(self):
        self._idx = 0
        return self

    def __next__(self):
        if self._idx >= len(self.positions):
            raise StopIteration

        retour = self.grille.cases[self.positions[self._idx]]
        self._idx += 1
        return retour


class Grille:

    def __init__(self, base):
        self.base = base

        taille = base.nb_cases()
        self.cases = [None] * taille
        for i in range(taille):
            self.cases[i] = Case()

    def __eq__(self, autre):
        return (self.base == autre.base and self.cases == autre.cases)

    def __hash__(self):
        return hash(str(self))

    def __repr__(self):
        # On détermine la longueur des champs à afficher
        lgv = len(
            str(max([c.valeur for c in self.cases if c.valeur >= 1],
                    default=1)))
        lgr = len(
            str(max([c.région for c in self.cases if c.région >= 0],
                    default=1)))
        lgc = 3 + lgv + lgr

        # Constitution de la grille, ligne à ligne
        lignes = list()
        for h in range(self.base.hauteur):
            # Séparateur horizontal
            ligne = "+"
            for l in range(self.base.largeur):
                séparateur = " "
                if h == 0:
                    séparateur = "-"
                else:
                    r = self[(h, l)].région
                    if r >= 0 and self[(h - 1, l)].région != r:
                        séparateur = "-"
                ligne += séparateur * lgc + "+"
            lignes.append(ligne)

            # Cases
            ligne = str()
            for l in range(self.base.largeur):
                case = self[(h, l)]
                séparateur = " "
                if l == 0:
                    séparateur = "|"
                else:
                    r = case.région
                    if r >= 0 and self[(h, l - 1)].région != r:
                        séparateur = "|"
                ligne += séparateur
                if case.valeur >= 1:
                    v = f"{case.valeur:{lgv}}"
                else:
                    v = " " * lgv
                if case.région >= 0:
                    r = f"{case.région:{lgr}}"
                else:
                    r = " " * lgr
                ligne += f"({v},{r})"
            ligne += "|"
            lignes.append(ligne)

        # Dernier séparateur horizontal
        ligne = "+" + ("-" * lgc + "+") * self.base.largeur
        lignes.append(ligne)

        retour = "\n".join(lignes)
        return retour

    def __str__(self):
        # On détermine la longueur des champs à afficher
        lgv = len(
            str(max([c.valeur for c in self.cases if c.valeur >= 1],
                    default=1)))
        lgc = lgv

        # Constitution de la grille, ligne à ligne
        lignes = list()
        for h in range(self.base.hauteur):
            # Séparateur horizontal
            ligne = "+"
            for l in range(self.base.largeur):
                séparateur = " "
                if h == 0:
                    séparateur = "-"
                else:
                    r = self[(h, l)].région
                    if r >= 0 and self[(h - 1, l)].région != r:
                        séparateur = "-"
                ligne += séparateur * lgc + "+"
            lignes.append(ligne)

            # Cases
            ligne = str()
            for l in range(self.base.largeur):
                case = self[(h, l)]
                séparateur = " "
                if l == 0:
                    séparateur = "|"
                else:
                    r = case.région
                    if r >= 0 and self[(h, l - 1)].région != r:
                        séparateur = "|"
                ligne += séparateur
                if case.valeur >= 1:
                    v = f"{case.valeur:{lgv}}"
                else:
                    v = " " * lgv
                ligne += v
            ligne += "|"
            lignes.append(ligne)

        # Dernier séparateur horizontal
        ligne = "+" + ("-" * lgc + "+") * self.base.largeur
        lignes.append(ligne)

        retour = "\n".join(lignes)
        return retour

    def __getitem__(self, position):
        h, l = position
        index = self.base.en_index(hauteur=h, largeur=l)
        return self.cases[index]

    def __setitem__(self, position, valeur):
        h, l = position
        index = self.base.en_index(hauteur=h, largeur=l)
        self.cases[index] = valeur

    @staticmethod
    def depuis_chaîne(chaîne):
        """Recrée une Grille à partir de sa forme «chaîne de caractères»
        """
        lignes = chaîne.split("\n")
        while len(lignes[-1]) == 0:
            del lignes[-1]

        # Création d'une grille vierge
        hauteur = len(lignes) // 2
        largeur = lignes[0].count('+') - 1
        maximum = hauteur * largeur
        base = Base.singleton(hauteur=hauteur,
                              largeur=largeur,
                              maximum=maximum)
        retour = Grille(base)

        # On applique la même stratégie que pour la création de labyrinthes,
        # en fusionnant les régions qui ont un côté commun
        régions = dict()
        for h in range(hauteur):
            for l in range(largeur):
                p = (h, l)

                if lignes[2 * h + 1][2 * l + 1] != ' ':
                    retour[p].valeur = int(lignes[2 * h + 1][2 * l + 1])

                régions[p] = set([p])
                fusionné = False
                if l > 0 and lignes[2 * h + 1][2 * l] == ' ':
                    régions[p] = régions[(h, l - 1)]
                    régions[p].add(p)
                    fusionné = True
                if h > 0 and lignes[2 * h][2 * l + 1] == ' ':
                    if not fusionné:
                        régions[p] = régions[(h - 1, l)]
                        régions[p].add(p)
                    else:
                        réf = régions[(h - 1, l)]
                        for q in régions[p]:
                            régions[q] = réf
                        réf.update(régions[p])

        maximum = 0
        id_régions = []
        for région in régions.values():
            if région not in id_régions:
                id_régions.append(région)
            if len(région) > maximum:
                maximum = len(région)
        for h in range(hauteur):
            for l in range(largeur):
                p = (h, l)
                id = id_régions.index(régions[p])
                retour[p].région = id
        retour.base.maximum = maximum

        return retour

    def nb_régions(self):
        """Nombre de régions différentes identifiées
        """
        régions = set([c.région for c in self.cases if c.région >= 0])
        return len(régions)

    def région(self, id_région):
        positions = list()
        for i, c in enumerate(self.cases):
            if c.région == id_région:
                positions.append(self.base.en_position(i))
        return Région(self, positions)

    def régions(self):
        """Itérateur de toutes les Régions
        """
        ids = sorted(set([c.région for c in self.cases]))
        for id in ids:
            yield self.région(id)

    def est_canonique(self):
        """Vrai ssi la grille est une forme canonique

        La forme canonique permet d'éliminer les symétries
        """
        # Premier filtre : les valeurs des cases
        for h in range(self.base.hauteur // 2):
            for l in range(self.base.largeur // 2):
                coords = [(h, l), (h, self.base.largeur - 1 - l),
                          (self.base.hauteur - 1 - h, l),
                          (self.base.hauteur - 1 - h,
                           self.base.largeur - 1 - l)]
                cases = [self[p].valeur for p in coords]
                valeurs = [v for v in cases[1:] if v >= 1]
                if len(valeurs) == 0:
                    if cases[0] >= 1:
                        return True
                else:
                    for v in valeurs:
                        if v < cases[0]:
                            return False  # cases[0] > min(valeurs)
                        elif v > cases[0]:
                            return True  # cases[0] < max(valeurs)

        # TODO Second filtre : la structure

        return True

    def est_normale(self):
        """Vrai ssi la grille respecte la forme normale
        """
        # Attribution de nouveaux numéros
        # anciens[r] est l'ancien numéro de la région désormais identifiée 'r'
        retour = True
        anciens = list()
        for case in self.cases:
            région = case.région
            if région >= 0:
                try:
                    nouvelle_région = anciens.index(région)
                except ValueError:
                    nouvelle_région = len(anciens)
                    anciens.append(région)
                if nouvelle_région != case.région:
                    retour = False
                    break
        return retour

    def normaliser(self):
        """Assure un ordre de numérotation entre Régions.

        La valeur de retour indique si une modification a été effectuée
        """
        # Attribution de nouveaux numéros
        # anciens[r] est l'ancien numéro de la région désormais identifiée 'r'
        utile = False
        anciens = list()
        for case in self.cases:
            région = case.région
            if région >= 0:
                try:
                    nouvelle_région = anciens.index(région)
                except ValueError:
                    nouvelle_région = len(anciens)
                    anciens.append(région)
                if nouvelle_région != case.région:
                    utile = True
                case.région = nouvelle_région
        return utile

    def est_complète(self):
        """Vrai ssi toutes les cases ont une valeur de fixée
        """
        for case in self.cases:
            if case.valeur < 1:
                return False
        return True

    def transposer(self):
        """Interversion des largeur et hauteur
        """
        base = self.base.transposée()
        cases = [None] * self.base.nb_cases()
        for i in range(len(cases)):
            h, l = base.en_position(i)
            j = self.base.en_index(hauteur=h, largeur=l)
            cases[i] = self.cases[j]

        self.base = base
        self.cases[:] = cases


class Lecteur:
    """Itérateur de codes généralement issus de l'entrée standard
    """

    def __init__(self, base, codes):
        self.chemin = None

        self.base = base
        self.codes = codes

        self._i = 0

    @property
    def nb_codes(self):
        return len(self.codes)

    def __iter__(self):
        self._i = 0
        return self

    def __next__(self):
        if self._i == len(self.codes):
            raise StopIteration
        else:
            retour = self.codes[self._i]
            self._i += 1
            return retour
