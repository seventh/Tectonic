# -*- coding:utf-8 -*-
"""Format de fichier binaire

Définition de l'en-tête (>13 octets):

  +---+-------------------------------------------------+
  | s | "TECTONIC"                                      |
  | B | numéro de version : '\\x02'                      |
  | B | Base.hauteur                                    |
  | B | Base.largeur                                    |
  | B | Base.maximum                                    |
  | B | nombre d'octets Y pour chaque "nombre de codes" |
  | Y | nombre total de codes                           |
  +---+-------------------------------------------------+

Définition d'un bloc:

  +---+------------------------------------+
  | B | nombre n de paires taille×nombre   |
  | B | taille '1'                         |
  | Y | nombre de codes pour la taille '1' |
  | B | taille '2'                         |
  | Y | nombre de codes pour la taille '2' |
  | … | …                                  |
  | B | taille 'n'                         |
  | Y | nombre de codes pour la taille 'n' |
  +---+------------------------------------+

  puis:

   segment des codes de longeur taille[1] bits
   segment des codes de longeur taille[2] bits
   …
   segment des codes de longeur taille[n] bits

  Des bits nuls sont ajoutés à la fin de chaque segment pour occuper un nombre
  entier d'octets.

  Chaque code est à reconstruire à partir de taille[i-1] bits :
    - on préfixe un bit de poids fort '1'
    - on ajoute 1 au nombre représenté
  L'ordre dans lequel on fait ces deux opérations importe peu a priori

Définition du marqueur de fin (1 octet):

  +---+---------------------------------------+
  | B | marqueur de fin : '\\b00000000'        |
  +---+---------------------------------------+

  Le marqueur de fin est équivalent à un bloc vide
"""

import os
import struct

from . import Base


def entier_en_binaire(taille, entier):
    return entier.to_bytes(length=taille, byteorder="big", signed=False)


def binaire_en_entier(binaire):
    return int.from_bytes(binaire, byteorder="big", signed=False)


# Chaque segment peut être décomposé en une succession de morceaux de 8
# codes. En fonction de la longueur des codes du segment, on a un nombre
# entier de codes dans un morceau.


class Segment:
    """Description d'un Segment

    Objet immuable : les attributs d'une instance ne doivent pas être
    modifiés.
    """

    def __init__(self, taille, nombre):
        self._taille = taille
        self._nombre = nombre

    @property
    def taille(self):
        """Nombre de bits des codes constituants ce Segment
        """
        return self._taille

    @property
    def nombre(self):
        """Nombre de codes constituants ce Segment
        """
        return self._nombre

    def nb_octets(self):
        """Nombre d'octets nécessaires pour représenter ce Segment
        """
        nb_bits = self.taille * (self.nombre - 1)
        retour, reste = divmod(nb_bits, 8)
        if reste != 0:
            retour += 1
        return retour

    def taille_morceau(self):
        """Nombre d'octets d'un morceau de 8 codes
        """
        return (self.taille - 1)


class Codec:
    """Transformation entre morceau et liste de codes
    """

    def __init__(self, nb_bits):
        self.lg = nb_bits - 1
        self.masque = (1 << self.lg)
        self.format = f"{{:0>{self.lg}b}}"

    def _set_bit(self, code):
        return code | self.masque

    def _clear_bit(self, code):
        return code & ~self.masque

    def décoder(self, morceau: bytes):
        # Conversion au format binaire
        binaire = "".join(f"{octet:08b}" for octet in morceau)

        # Découpage en codes
        retour = list()
        i = 0
        j = self.lg
        while j <= len(binaire):
            # Théoriquement, le "+1" devrait être fait APRÈS le positionnement
            # du bit de poids fort. Cependant, le "-1" fait au moment de
            # l'encodage invalide l'hypothèse que celui-ci est positionné :
            #
            #  > 1000 - 0001 → 0111         # changement du bit de poids fort
            #  > clear_bit (0111) → 0111    # pas d'impact !
            #
            # Si la soustraction, faite ici pour éviter de modifier (pour
            # l'instant) l'algorithme de sérialisation des grilles, était
            # prise en charge directement par ce dernier, alors les opérations
            # d'encodage et de décodage seraient effectivement parfaitement
            # symétriques.
            code = self._set_bit(int(binaire[i:j], 2) + 1)
            assert code.bit_length() == self.lg + 1
            retour.append(code)
            i = j
            j += self.lg

        # Vérification finale
        suffixe = binaire[i:]
        assert len(suffixe) < 8
        assert "1" not in suffixe

        # Sortie
        return retour

    def encoder(self, codes):
        binaires = list()
        for code in codes:
            assert code.bit_length() == self.lg + 1
            binaire = self.format.format(self._clear_bit(code - 1))
            binaires.append(binaire)
        nb_bits = self.lg * len(codes)
        nb_octets = (nb_bits + 7) // 8
        binaires.append((8 * nb_octets - nb_bits) * '0')
        valeur = int("0b" + "".join(binaires), 2)
        retour = valeur.to_bytes(nb_octets, "big")

        return retour


class Écrivain:
    """Format ressemblant à un flux
    """

    FORMAT = 2

    def __init__(self,
                 chemin,
                 base,
                 bloc=0,
                 *,
                 reprise=False,
                 progressif=False):
        """Prépare une nouvelle sortie

        'bloc' détermine le nombre de codes par bloc. Si < 1, on cherchera
        à minimiser le nombre de blocs.

        Si 'reprise' est vrai, alors les futurs codes seront ajoutés au
        fichier s'il existe déjà. Sinon, le fichier est purgé avant toute
        écriture.

        Si 'progressif' est vrai, alors les blocs sont agencés de telle
        manière que le dernier code soit en dernière position.
        """
        assert bloc >= 0, "Valeur positive ou nulle"
        self.bloc = bloc

        self.progressif = progressif
        self._dernière_taille = None

        if reprise and os.path.exists(chemin):
            self.sortie = open(chemin, "r+b")

            # Lecture du nombre de codes
            self.sortie.seek(12, os.SEEK_SET)
            tampon = self.sortie.read(8)
            self._nb_codes = struct.unpack(">Q", tampon)[0]

            # Repositionnement de la tête d'écriture
            self.sortie.seek(-1, os.SEEK_END)
        else:
            self.sortie = open(chemin, "wb")

            # Écriture de l'en-tête
            self.sortie.write(b"TECTONIC\x01")
            self.sortie.write(
                struct.pack(">BBB", base.hauteur, base.largeur, base.maximum))

            # Réservation de 8 octets pour la taille
            self.sortie.write(b"\x00" * 8)

            self._nb_codes = 0

        # Le cache est par taille de code (en octets)
        self._nb_codes_en_attente = 0
        self.cache = dict()

    @property
    def nb_codes(self):
        """Nombre total d'enregistrements disponibles
        """
        return self._nb_codes + self._nb_codes_en_attente

    def purger(self):
        """Transfère les codes en cache dans le fichier
        """
        tailles = sorted(self.cache)
        if self.progressif and self._dernière_taille is not None:
            tailles.remove(self._dernière_taille)
            tailles.append(self._dernière_taille)

        while len(tailles) > 0:
            # Marqueur de structure
            nb_tailles = min(127, len(tailles))
            self.sortie.write(struct.pack(">B", nb_tailles))

            # Structure
            for taille in tailles[:nb_tailles]:
                self.sortie.write(
                    struct.pack(">BL", taille, len(self.cache[taille])))

            # Contenu
            for taille in tailles[:nb_tailles]:
                for code in self.cache[taille]:
                    self.sortie.write(
                        code.to_bytes(length=taille,
                                      byteorder="big",
                                      signed=False))
                self._nb_codes += len(self.cache[taille])

            # Itération
            del tailles[:nb_tailles]

        # Suppression du cache
        self._nb_codes_en_attente = 0
        self.cache.clear()
        self._dernière_taille = None

    def ajouter(self, code):
        nb_bits = max(1, code.bit_length())
        nb_octets = (nb_bits + 7) // 8
        self.cache.setdefault(nb_octets, list()).append(code)
        self._nb_codes_en_attente += 1
        self._dernière_taille = nb_octets

        if self._nb_codes_en_attente == self.bloc:
            self.purger()

    def clore(self):
        if self._nb_codes_en_attente != 0:
            self.purger()

        # Écriture du marqueur de fin de flux
        self.sortie.write(struct.pack(">B", 128))

        # Écriture de la taille
        self.sortie.seek(12, os.SEEK_SET)
        self.sortie.write(struct.pack(">Q", self._nb_codes))

        # Fermeture
        self.sortie.close()
        self.sortie = None


class Lecteur:

    FORMAT = 2

    def __init__(self, chemin):
        self.chemin = chemin
        self.entrée = open(chemin, "rb")

        # Vérification du prélude
        prélude = self.entrée.read(9)
        assert prélude == b"TECTONIC\x02"

        # Dimensions
        tampon = self.entrée.read(3)
        hauteur, largeur, maximum = struct.unpack(">3B", tampon)
        self._base = Base.singleton(largeur=largeur,
                                    hauteur=hauteur,
                                    maximum=maximum)

        # Paramètre «Y»
        tampon = self.entrée.read(1)
        self._y_dim = struct.unpack(">B", tampon)[0]

        # Nombre de codes
        self.nb_codes = self._lire_nombre()

        # Analyse du contenu
        self._analyser()

        # Préparation de l'itération
        self.fin_rencontrée = False
        self.id_code = 0
        self.cache = list()

    def _lire_nombre(self):
        tampon = self.entrée.read(self._y_dim)
        return binaire_en_entier(tampon)

    def _analyser(self):
        pass

    @property
    def base(self):
        """Base commune à tous les codes
        """
        return self._base

    def __len__(self):
        return self.nb_codes - self.id_code

    def __iter__(self):
        self.entrée.seek(20, os.SEEK_SET)
        self.fin_rencontrée = False
        self.id_code = 0
        self.cache.clear()
        return self

    def __next__(self):
        if len(self.cache) == 0:
            self._charger()
        if self.fin_rencontrée:
            raise StopIteration
        else:
            taille, nombre = self.cache[0]
            retour = int.from_bytes(self.entrée.read(taille),
                                    byteorder="big",
                                    signed=False)
            self.id_code += 1
            nombre -= 1
            if nombre == 0:
                del self.cache[0]
            else:
                self.cache[0] = (taille, nombre)
            return retour

    def _charger(self):
        marqueur = struct.unpack(">B", self.entrée.read(1))[0]
        if marqueur > 127:
            self.fin_rencontrée = True
        else:
            for k in range(marqueur):
                paire = struct.unpack(">BL", self.entrée.read(5))
                self.cache.append(paire)

    def seek(self, offset, whence=os.SEEK_SET):
        """Pointe la tête de lecture sur le code voulu
        """
        # On détermine l'identifiant du code désigné
        if whence == os.SEEK_SET:
            id_code = offset
        elif whence == os.SEEK_CUR:
            id_code = self.id_code + offset
        elif whence == os.SEEK_END:
            id_code = self.nb_codes + offset
        if id_code < 0 or id_code >= self.nb_codes:
            raise ValueError

        # Version très peu efficace : on itère les codes un à un jusqu'à
        # tomber sur celui voulu
        if id_code < self.id_code:
            iter(self)
        for _ in range(id_code - self.id_code):
            next(self)

    def tell(self):
        """Position actuelle de la tête de lecture
        """
        return self.id_code
