# -*- coding:utf-8 -*-
"""Format de fichier binaire

Définition de l'en-tête (20 octets):

  +---+---------------------------------------+
  | s | "TECTONIC"                            |
  | B | numéro de version : '\\x01'            |
  | B | Base.hauteur                          |
  | B | Base.largeur                          |
  | B | Base.maximum                          |
  | Q | nombre total de codes                 |
  +---+---------------------------------------+

Définition d'un bloc:

  +---+---------------------------------------+
  | B | nombre de paires taille×nombre        |
  | B | taille '1'                            |
  | L | nombre de codes pour la taille '1'    |
  | B | taille '2'                            |
  | L | nombre de codes pour la taille '2'    |
  | … | …                                     |
  | B | taille 'n'                            |
  | L | nombre de codes pour la taille 'n'    |
  +---+---------------------------------------+

  puis:

   nombre[1] codes de longueur taille[1]
   nombre[2] codes de longueur taille[2]
   …
   nombre[n] codes de longueur taille[n]

Définition du marqueur de fin (1 octet):

  +---+---------------------------------------+
  | B | marqueur de fin : 0                   |
  +---+---------------------------------------+
"""

import enum
import os
import struct

from . import Base


class Segment:
    """Description d'un Segment de codes

    Objet immuable : les attributs d'une instance ne doivent pas être
    modifiés.
    """

    def __init__(self, avance, nombre, taille):
        self._avance = avance
        self._nombre = nombre
        self._taille = taille

    def __repr__(self):
        return (f"Segment[0x{id(self):x}]"
                f"(avance={self.avance},"
                f" nombre={self.nombre},"
                f" taille={self.taille})")

    def __str__(self):
        return (f"Segment"
                f"(avance={self.avance},"
                f" nombre={self.nombre},"
                f" taille={self.taille})")

    @property
    def avance(self):
        """Position du premier code
        """
        return self._avance

    @property
    def nombre(self):
        """Nombre de codes
        """
        return self._nombre

    @property
    def taille(self):
        """Nombre d'octets occupé par chaque code
        """
        return self._taille

    def limite(self):
        """Position de fin
        """
        return self._avance + self.volume()

    def volume(self):
        """Nombre total d'octets occupés
        """
        return self._nombre * self._taille


def entier_en_binaire(taille: int, entier: int) -> bytes:
    return entier.to_bytes(length=taille, byteorder="big", signed=False)


def binaire_en_entier(binaire: bytes) -> int:
    return int.from_bytes(binaire, byteorder="big", signed=False)


def analyser(entrée):
    """Identifie les différents segments du fichier

    La tête de lecture est remise en place à l'issue de l'analyse.
    """
    retour = list()
    initial = entrée.tell()

    # Parcours de la structure
    avance = entrée.seek(20, os.SEEK_SET)
    while True:
        n = binaire_en_entier(entrée.read(1))
        if n == 0:
            break
        avance += 1 + 5 * n
        for _ in range(n):
            taille = binaire_en_entier(entrée.read(1))
            nombre = binaire_en_entier(entrée.read(4))
            s = Segment(avance, nombre, taille)
            avance += s.volume()
            retour.append(s)
        entrée.seek(retour[-1].limite(), os.SEEK_SET)

    entrée.seek(initial, os.SEEK_SET)
    return retour


class Rangement(enum.Enum):
    """Manière de stocker les codes dans un fichier
    """

    # La lecture des codes est garantie s'effectuer exactement dans le même
    # ordre que l'écriture. Des optimisations locales peuvent avoir lieu du
    # moment que cette garantie est maintenue (deux codes successifs de même
    # taille peuvent partager le même segment)
    DIRECT = enum.auto()

    # Des regroupements sont autorisés afin de minimiser le nombre de segments
    # dans chaque bloc. Le dernier code du bloc est le dernier dont l'écriture
    # a été demandée (donc, son segment aussi)
    GROUPE = enum.auto()

    # Dans chaque bloc, les codes sont ordonnés par valeur croissante, tout en
    # minimisant le nombre de segments.
    NORMAL = enum.auto()


class Écrivain:
    """Format ressemblant à un flux
    """

    FORMAT = 1

    def __init__(self,
                 chemin,
                 base,
                 bloc=0,
                 *,
                 reprise=False,
                 rangement=Rangement.GROUPE):
        """Prépare une nouvelle sortie

        'bloc' détermine le nombre de codes par bloc. Si < 1, on cherchera
        à minimiser le nombre de blocs.

        Si 'reprise' est vrai, alors les futurs codes seront ajoutés au
        fichier s'il existe déjà. Sinon, le fichier est purgé avant toute
        écriture.

        Les codes seront stockés en respectant la politique de `rangement`.
        """
        assert bloc >= 0, "Valeur positive ou nulle"
        self.bloc = bloc

        self.rangement = rangement
        self._dernière_taille = None

        if reprise and os.path.exists(chemin):
            self.sortie = open(chemin, "r+b")

            # Lecture du nombre de codes
            self.sortie.seek(12, os.SEEK_SET)
            self._nb_codes = binaire_en_entier(self.sortie.read(8))

            # Repositionnement de la tête d'écriture
            self.sortie.seek(-1, os.SEEK_END)
        else:
            self.sortie = open(chemin, "wb")

            # Écriture de l'en-tête
            self.sortie.write(b"TECTONIC\x01")
            self.sortie.write(entier_en_binaire(1, base.hauteur))
            self.sortie.write(entier_en_binaire(1, base.largeur))
            self.sortie.write(entier_en_binaire(1, base.maximum))

            # Réservation de 8 octets pour la taille
            self.sortie.write(entier_en_binaire(8, 0))

            self._nb_codes = 0

        # Le cache est par taille de code (en octets). Pour avoir une approche
        # homogène quelque soit la politique de rangement, on utilise deux
        # listes plutôt qu'un dictionnaire par taille, qui certes garantirait
        # l'unicité des tailles, mais ne conviendrait pas en mode DIRECT.
        self._nb_codes_en_attente = 0
        self._segments = list()
        self._tailles = list()

    @property
    def nb_codes(self):
        """Nombre total d'enregistrements disponibles
        """
        return self._nb_codes + self._nb_codes_en_attente

    def purger(self):
        """Transfère les codes en cache dans le fichier
        """
        # Tous les segments créés par `ajouter` respectent la contrainte de
        # longueur maximale.

        # Réorganisation des codes en fonction de ce qu'autorise la politique
        # de rangement
        if self.rangement is not Rangement.DIRECT:
            # Tri par taille croissante des différents segments
            index = list(range(len(self._tailles)))
            index.sort(key=lambda v: self._tailles[v])
            self._tailles[:] = [self._tailles[i] for i in index]
            self._segments[:] = [self._segments[i] for i in index]
            # Éventuellement, on trie le tout
            if self.rangement is Rangement.NORMAL:
                for s in self._segments:
                    s.sort()
            elif self._dernière_taille is not None:
                # self.rangement is Rangement.GROUPE
                index = self._tailles.index(self._dernière_taille)
                self._tailles[index], self._tailles[-1] = self._tailles[
                    -1], self._tailles[index]
                self._segments[index], self._segments[-1] = self._segments[
                    -1], self._segments[index]

        # Création de blocs
        i = 0
        while i < len(self._tailles):
            # Marqueur de bloc
            j = min(i + 255, len(self._tailles))
            nb_segments = j - i
            self.sortie.write(entier_en_binaire(1, nb_segments))

            # Structure de bloc
            for k in range(i, j):
                self.sortie.write(entier_en_binaire(1, self._tailles[k]))
                self.sortie.write(entier_en_binaire(4, len(self._segments[k])))

            # Segments
            for k in range(i, j):
                taille = self._tailles[k]
                for code in self._segments[k]:
                    self.sortie.write(entier_en_binaire(taille, code))
                self._nb_codes += len(self._segments[k])

            # Itération
            i = j

        # Suppression du cache
        self._nb_codes_en_attente = 0
        self._tailles.clear()
        self._segments.clear()
        self._dernière_taille = None

    def ajouter(self, code):
        # La fonction d'ajout s'assure simplement qu'il n'y a pas trop de codes
        # par segment. `purger`, de son côté, a pour charge de ne pas créer de
        # blocs constitués de trop de segments.

        nb_bits = max(1, code.bit_length())
        taille = (nb_bits + 7) // 8

        # Recherche du dernier segment de taille correspondante s'il existe en
        # cache
        i = len(self._tailles) - 1
        if self.rangement is Rangement.DIRECT:
            if i >= 0 and self._tailles[i] != taille:
                i = -1
        else:
            while i >= 0 and self._tailles[i] != taille:
                i -= 1

        # Si le segment trouvé est déjà plein, on ne doit évidemment pas y
        # ajouter un autre code
        if i >= 0 and len(self._segments[i]) >= 2**32:
            i = -1

        # Création d'un nouveau segment si nécessaire
        if i >= 0:
            self._segments[i].append(code)
        else:
            self._tailles.append(taille)
            self._segments.append([code])
        self._dernière_taille = taille
        self._nb_codes_en_attente += 1
        if self._nb_codes_en_attente == self.bloc:
            self.purger()

    def clore(self):
        if self._nb_codes_en_attente != 0:
            self.purger()

        # Écriture du marqueur de fin de flux
        self.sortie.write(entier_en_binaire(1, 0))

        # Écriture de la taille
        self.sortie.seek(12, os.SEEK_SET)
        self.sortie.write(entier_en_binaire(8, self._nb_codes))

        # Fermeture
        self.sortie.close()
        self.sortie = None


class Lecteur:

    FORMAT = 1

    def __init__(self, chemin):
        self.chemin = chemin
        self.entrée = open(chemin, "rb")

        # Vérification du prélude
        prélude = self.entrée.read(9)
        assert prélude == b"TECTONIC\x01"

        # Dimensions
        tampon = self.entrée.read(3)
        hauteur, largeur, maximum = struct.unpack(">3B", tampon)
        self._base = Base.singleton(largeur=largeur,
                                    hauteur=hauteur,
                                    maximum=maximum)

        # Nombre de codes
        self.nb_codes = binaire_en_entier(self.entrée.read(8))

        # Préparation de l'itération
        self.fin_rencontrée = False
        self.id_code = 0
        self.cache = list()

    @property
    def base(self):
        """Base commune à tous les codes
        """
        return self._base

    def __len__(self):
        return self.nb_codes - self.id_code

    def __iter__(self):
        self.entrée.seek(20, os.SEEK_SET)
        self.fin_rencontrée = False
        self.id_code = 0
        self.cache.clear()
        return self

    def __next__(self):
        if len(self.cache) == 0:
            self._charger()
        if self.fin_rencontrée:
            raise StopIteration
        else:
            taille, nombre = self.cache[0]
            retour = binaire_en_entier(self.entrée.read(taille))
            self.id_code += 1
            nombre -= 1
            if nombre == 0:
                del self.cache[0]
            else:
                self.cache[0] = (taille, nombre)
            return retour

    def _charger(self):
        marqueur = binaire_en_entier(self.entrée.read(1))
        if marqueur == 0:
            self.fin_rencontrée = True
        else:
            for k in range(marqueur):
                paire = struct.unpack(">BL", self.entrée.read(5))
                self.cache.append(paire)

    def seek(self, offset, whence=os.SEEK_SET):
        """Pointe la tête de lecture sur le code voulu
        """
        # On détermine l'identifiant du code désigné
        if whence == os.SEEK_SET:
            id_code = offset
        elif whence == os.SEEK_CUR:
            id_code = self.id_code + offset
        elif whence == os.SEEK_END:
            id_code = self.nb_codes + offset
        if id_code < 0 or id_code >= self.nb_codes:
            raise ValueError

        # Version très peu efficace : on itère les codes un à un jusqu'à
        # tomber sur celui voulu
        if id_code < self.id_code:
            iter(self)
        for _ in range(id_code - self.id_code):
            next(self)

    def tell(self):
        """Position actuelle de la tête de lecture
        """
        return self.id_code
