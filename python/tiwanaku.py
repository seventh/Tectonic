#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Générateur de grille pour Tiwanaku

Il s'agit tout à la fois de produire :
- une grille de Tectonic aux bonnes dimensions, d'une difficulté acceptable
- de colorer les zones
- d'en proposer une vue sans les zones
"""

import enum
import getopt
import logging
import sys

from tectonic import Base


class Configuration:
    """Configuration pour le script

    --petit  Génère une petite grille pour 2 joueurs (5×5)
    --grand  Génère une grande grille (9×5)
    """

    def __init__(self):
        self.debug = False
        self.base = Base(hauteur=5, largeur=5, maximum=5)

    @staticmethod
    def charger():
        """Construction depuis la ligne de commande
        """
        retour = Configuration()

        largeur = 5
        opts, args = getopt.getopt(sys.argv[1:], "g", ["petit", "grand"])
        for opt, val in opts:
            if opt == "-g":
                retour.debug = True
            elif opt == "--grand":
                largeur = 9
            elif opt == "--petit":
                largeur = 5
        if len(args) != 0:
            logging.warning(f"Arguments ignorés : {args}")

        if retour.base.largeur != largeur:
            retour.base = Base(hauteur=5, largeur=largeur, maximum=5)

        return Configuration()


if __name__ == "__main__":
    CONF = Configuration.charger()
    print(f"Génération d'une grille {CONF.base}…")
