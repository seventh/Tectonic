#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import unittest

from tectonic.fichier_002 import Codec


class CodecTestCase(unittest.TestCase):

    def testAllerRetour(self):
        codec = Codec(3)
        originaux = [4, 6, 5]
        morceau = codec.encoder(originaux)
        self.assertTrue(isinstance(morceau, bytes))
        self.assertEqual(len(morceau), 1)
        codes = codec.décoder(morceau)
        self.assertTrue(isinstance(codes, list))
        self.assertEqual(codes[:len(originaux)], originaux)


if __name__ == "__main__":
    unittest.main()
