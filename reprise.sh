#!/bin/bash

for f in data/*.prg
do
    n=$( basename "$f" )
    h=$( echo $n | cut -c '2-3' )
    l=$( echo $n | cut -c '5-6' )
    m=$( echo $n | cut -c '8-9' )
    cd python ; ./gen_prof.py -h $h -l $l -m $m &
done
